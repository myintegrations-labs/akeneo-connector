
MyIntegrationsConnectorBundle
=====================

MyIntegrations for Akeneo PIM >= 2.0

With this connector you can connect your data with all your other platforms.

## Requirements

| MyIntegrationsConnectorBundle   | Akeneo PIM Community Edition |
|:-------------------------------:|:----------------------------:|
| v1.0.*                          | v2.*                         |

## Connector installation on Akeneo PIM

If it's not already done, install [Akeneo PIM](https://github.com/akeneo/pim-community-standard).

Get composer (with command line):
```console
$ cd /my/pim/installation/dir
$ curl -sS https://getcomposer.org/installer | php
```

Then, install the module with composer:

In your ```composer.json```, add the following code dependending on your Akeneo PIM version:

* In `require`:

```json
{
    "myintegrations/akeneo-connector-bundle": "^1.0"
}
```

Next, enter the following command line:
```console
$php composer.phar require myintegrations/akeneo-connector-bundle
```

Enable the bundle in ```app/AppKernel.php``` file, in the ```registerBundles``` function, before the line ```return $bundles```:
```php
$bundles[] = new MyIntegrations\Bundle\ConnectorBundle\MyIntegrationsConnectorBundle();
```
Add the route in ```app/routing.yml```file, under the _pim_reference_data_ node:
```yml
my_integrations_connector:
    prefix: /myintegrations
    resource: "@MyIntegrationsConnectorBundle/Resources/config/routing.yml"
```
