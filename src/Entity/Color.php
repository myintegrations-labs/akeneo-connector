<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Entity;

use Pim\Component\ReferenceData\Model\AbstractReferenceData;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use MyIntegrations\Bundle\ConnectorBundle\Entity\AbstractCustomEntity;
use MyIntegrations\Bundle\ConnectorBundle\Versioning\VersionableInterface;

/**
 * @UniqueEntity("code")
 */
class Color extends AbstractCustomEntity
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $code;

    /** @var string */
    protected $hex;

    /** @var int */
    protected $red;

    /** @var int */
    protected $green;

    /** @var int */
    protected $blue;

    /** @var string */
    protected $type;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Color
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getHex()
    {
        return $this->hex;
    }

    /**
     * @param string $hex
     */
    public function setHex($hex)
    {
        $this->hex = $hex;
    }

    /**
     * @return int
     */
    public function getRed()
    {
        return $this->red;
    }

    /**
     * @param int $red
     */
    public function setRed($red)
    {
        $this->red = $red;
    }

    /**
     * @return int
     */
    public function getGreen()
    {
        return $this->green;
    }

    /**
     * @param int $green
     */
    public function setGreen($green)
    {
        $this->green = $green;
    }

    /**
     * @return int
     */
    public function getBlue()
    {
        return $this->blue;
    }

    /**
     * @param int $blue
     */
    public function setBlue($blue)
    {
        $this->blue = $blue;
    }

    /**
     * {@inheritdoc}
     */
    public static function getLabelProperty(): string
    {
        return 'name';
    }

    public function getCustomEntityName(): string
    {
        return 'color';
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return 'color';
    }
}
