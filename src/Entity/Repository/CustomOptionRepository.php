<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Entity\Repository;

/**
 * Repository for the custom option entity
 *
 * @author    Antoine Guigan <antoine@akeneo.com>
 * @copyright 2013 Akeneo SAS (http://www.akeneo.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @deprecated will be removed in 1.7,
 *             please use \MyIntegrations\Bundle\ConnectorBundle\Entity\Repository\CustomEntityRepository
 */
class CustomOptionRepository extends CustomEntityRepository
{
}
