<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Traits;

use MyIntegrations\Bundle\ConnectorBundle\Entity\DataMapping;

/**
* step execution trait used for filtering and so
*/
trait DataMappingTrait
{
    protected function getMappingByCode($code, $entityName = self::AKENEO_ENTITY_NAME, $storeViewWise = true)
    {
        $params = [
            'code'       => $code,
            'entityType' => $entityName ,
            'apiUrl'     => $this->getApiUrl()
        ];
        if($storeViewWise && !empty($this->storeViewCode) && !in_array(self::AKENEO_ENTITY_NAME, ['option', 'family', 'product', 'category'])) {
            $params['storeViewCode'] = $this->storeViewCode;
        }

        $mapping = isset($this->mappingRepository) ? $this->mappingRepository->findOneBy($params) : null;

        return $mapping;
    }

    protected function addMappingByCode($code, $externalId, $relatedId = null, $entityName = self::AKENEO_ENTITY_NAME)
    {
        if($code) {
            $mapping =  new DataMapping();
            $mapping->setCode($code);
            $mapping->setEntityType($entityName);
            $mapping->setExternalId($externalId);
            $mapping->setRelatedId($relatedId);
            if(isset($this->stepExecution) && $this->stepExecution->getJobExecution()->getId()) {
                $mapping->setJobInstanceId($this->stepExecution->getJobExecution()->getId());
            }
            if(!empty($this->storeViewCode) && !in_array(self::AKENEO_ENTITY_NAME, ['option'])) {
                $mapping->setStoreViewCode($this->storeViewCode);
            }
            if($this->getApiUrl()) {
                $mapping->setApiUrl($this->getApiUrl());
            }

            $this->em->persist($mapping);
            $this->em->flush();

            return $mapping;
        }
    }

    protected function updateMappingByCode($code, $externalId, $relatedId, $entityName = self::AKENEO_ENTITY_NAME)
    {
        if($code) {
            $mapping = $this->getMappingByCode($code);
            if(!$mapping) {
                $mapping =  new DataMapping();
            }
            $mapping->setCode($code);
            $mapping->setEntityType($entityName);
            if(isset($this->stepExecution) && $this->stepExecution->getJobExecution()->getId()) {
                $mapping->setJobInstanceId($this->stepExecution->getJobExecution()->getId());
            }
            $mapping->setExternalId($externalId);
            $mapping->setRelatedId($relatedId);
            $mapping->setStoreViewCode($this->storeViewCode);

            if($this->getApiUrl()) {
                $mapping->setApiUrl($this->getApiUrl());
            }
            $this->em->persist($mapping);
            $this->em->flush();

            return $mapping;
        }
    }

    protected function deleteMapping($mapping)
    {
        if($mapping && $mapping instanceof DataMapping) {
            $this->em->remove($mapping);
            $this->em->flush();
        }

        return null;
    }

    private function getApiUrl()
    {
        return str_replace('https://', 'http://', $this->getHostName() );
    }
}
