<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Traits;

/**
* channel aware
*/
trait ChannelAwareTrait
{
    private $channelRepo;
    private $defaultCategory;

    private function getDefaultCategoryTreeId($parameters)
    {
        $category = $this->getDefaultCategory($parameters);
        return $category ? $category->getId() : null;
    }

    private function getDefaultCategoryTreeCode($parameters)
    {
        $category = $this->getDefaultCategory($parameters);
        return $category ? $category->getCode() : null;
    }

    private function getDefaultCategory($parameters)
    {
        if(!$this->defaultCategory && !empty($parameters)) {
            $filters = is_array($parameters) ? $parameters['filters'] : $parameters->get('filters');
            if(!empty($filters['structure']['scope'])) {
                $channelCode = $filters['structure']['scope'];
            } else if(isset($filters[0]['context']['scope'])) {
                $channelCode = $filters[0]['context']['scope'];
            } else {
                $channelCode = null;
            }

            if($channelCode) {
                $channel = $this->channelRepo->findOneByIdentifier($channelCode);
                $this->defaultCategory = $channel ? $channel->getCategory() : null;
            }
        }

        return $this->defaultCategory;
    }
}
