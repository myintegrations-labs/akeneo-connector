<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Job\JobParameters;

use Akeneo\Component\Batch\Job\JobInterface;
use Akeneo\Component\Batch\Job\JobParameters\ConstraintCollectionProviderInterface;
use Akeneo\Component\Batch\Job\JobParameters\DefaultValuesProviderInterface;
use Pim\Component\Connector\Validator\Constraints\ProductModelFilterData;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\NotBlank;
use Pim\Component\Catalog\Query\Filter\Operators;
use Pim\Component\Catalog\Validator\Constraints\Channel;
use Pim\Component\Connector\Validator\Constraints\FilterStructureLocale;
use Pim\Component\Connector\Validator\Constraints\FilterStructureAttribute;
use Pim\Component\Connector\Validator\Constraints\ProductFilterData;
use MyIntegrations\Bundle\ConnectorBundle\Component\Validator\ValidCredentials;
use Symfony\Component\Validator\Constraints\Type;
use Akeneo\Component\Localization\Localizer\LocalizerInterface;
use Pim\Component\Catalog\Repository\ChannelRepositoryInterface;
use Pim\Component\Catalog\Repository\LocaleRepositoryInterface;

class BaseExport implements
    ConstraintCollectionProviderInterface,
    DefaultValuesProviderInterface
{
    /** @var string[] */
    private $supportedJobNames;

    /**
     * @param string[]                              $supportedJobNames
     */
    public function __construct(
        ChannelRepositoryInterface $channelRepository,
        LocaleRepositoryInterface $localeRepository,
        array $supportedJobNames
    ) {
        $this->channelRepository = $channelRepository;
        $this->localeRepository  = $localeRepository;
        $this->supportedJobNames = $supportedJobNames;
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultValues()
    {
        $parameters = [
//            'with_media' => true,
//            'user_to_notify'    => null,
        ];

//        $parameters['decimalSeparator'] = LocalizerInterface::DEFAULT_DECIMAL_SEPARATOR;
//        $parameters['dateFormat'] = LocalizerInterface::DEFAULT_DATE_FORMAT;

        $channels = $this->channelRepository->getFullChannels();
        $defaultChannelCode = (0 !== count($channels)) ? $channels[0]->getCode() : null;

        $localesCodes = $this->localeRepository->getActivatedLocaleCodes();
        $defaultLocaleCodes = (0 !== count($localesCodes)) ? [$localesCodes[0]] : ['en_US'];

        $parameters['filters'] = [
            'data'      => [
                [
                    'field'    => 'enabled',
                    'operator' => Operators::EQUALS,
                    'value'    => true,
                ],
                [
                    'field'    => 'completeness',
                    'operator' => Operators::GREATER_OR_EQUAL_THAN,
                    'value'    => 100,
                ],
                [
                    'field'    => 'categories',
                    'operator' => Operators::IN_CHILDREN_LIST,
                    'value'    => []
                ]
            ],
            'structure' => [
                'scope'   => $defaultChannelCode,
                'locales' => $defaultLocaleCodes,
            ],
        ];

        return $parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function getConstraintCollection()
    {

        $constraintFields['user_to_notify'] = new Optional();

        // $constraintFields['filters'] = new Optional();
        /* more strict filter, structure contraint */
        $constraintFields['filters'] = [
            new ProductFilterData(['groups' => ['Default', 'DataFilters']]),
            new Collection(
                [
                    'fields'           => [
                        'structure' => [
                            new FilterStructureLocale(['groups' => ['Default', 'DataFilters']]),
                            new Collection(
                                [
                                    'fields'             => [
                                        'locales'    => new NotBlank(['groups' => ['Default', 'DataFilters']]),
                                        'scope'      => new Channel(['groups' => ['Default', 'DataFilters']]),
                                        'attributes' => new Type(
                                            [
                                                'type'   => 'array',
                                                'groups' => ['Default', 'DataFilters'],
                                            ]
                                        )
                                    ],
                                    'allowMissingFields' => true,
                                ]
                            ),
                        ],
                    ],
                    'allowExtraFields' => true,
                ]
            ),
        ];

        $constraintFields['product_only'] = new Optional();
        $constraintFields['with_media'] = new Optional();

        return new Collection(['fields' => $constraintFields]);
    }

    /**
     * {@inheritdoc}
     */
    public function supports(JobInterface $job)
    {
        return in_array($job->getName(), $this->supportedJobNames);
    }
}
