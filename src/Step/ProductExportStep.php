<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Step;

use Akeneo\Component\Batch\Item\FlushableInterface;
use Akeneo\Component\Batch\Item\InitializableInterface;
use Akeneo\Component\Batch\Item\InvalidItemException;
use Akeneo\Component\Batch\Item\ItemProcessorInterface;
use Akeneo\Component\Batch\Item\ItemReaderInterface;
use Akeneo\Component\Batch\Item\ItemWriterInterface;
use Akeneo\Component\Batch\Job\JobRepositoryInterface;
use Akeneo\Component\Batch\Model\StepExecution;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Akeneo\Component\Batch\Step\AbstractStep;
use Akeneo\Component\Batch\Step\StepExecutionAwareInterface;

/**
 * magento2 step implementation that read items, process them and write them using api, code in respective files
 *
 */
class ProductExportStep extends BaseStep
{
}
