<?php

namespace MyIntegrations\Bundle\ConnectorBundle;

use MyIntegrations\Bundle\ConnectorBundle\Action\ActionInterface;
use MyIntegrations\Bundle\ConnectorBundle\DependencyInjection\Compiler\RegisterProductQueryFilterPass;
use MyIntegrations\Bundle\ConnectorBundle\DependencyInjection\Compiler\RegisterSerializerPass;
use MyIntegrations\Bundle\ConnectorBundle\DependencyInjection\Compiler;
use MyIntegrations\Bundle\ConnectorBundle\DependencyInjection\Reference\ReferenceFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MyIntegrationsConnectorBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        $container
            ->addCompilerPass(new Compiler\ConfigurationBuilderPass())
            ->addCompilerPass(new Compiler\SerializerPass())
            ->addCompilerPass(new Compiler\ManagerRegistryPass())
            ->addCompilerPass(new Compiler\RegisterFormExtensionsPass());
//            ->addCompilerPass(new Compiler\RegisterGenericProvidersPass(new ReferenceFactory(), 'field'))
//            ->addCompilerPass(new Compiler\RegisterGenericProvidersPass(new ReferenceFactory(), 'empty_value'))
//            ->addCompilerPass(new Compiler\RegisterGenericProvidersPass(new ReferenceFactory(), 'form'))
//            ->addCompilerPass(new Compiler\RegisterGenericProvidersPass(new ReferenceFactory(), 'filter'))
//            ->addCompilerPass(new Compiler\RegisterCategoryItemCounterPass())
//            ->addCompilerPass(new RegisterSerializerPass('pim_internal_api_serializer'))
//            ->addCompilerPass(new RegisterProductQueryFilterPass('product_and_product_model'))


    }
}
