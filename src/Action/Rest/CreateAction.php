<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Action\Rest;

use MyIntegrations\Bundle\ConnectorBundle\Action\ActionFactory;
use MyIntegrations\Bundle\ConnectorBundle\Event\ActionEventManager;
use MyIntegrations\Bundle\ConnectorBundle\Manager\Registry as ManagerRegistry;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateAction extends AbstractRestAction
{
    /** @var ValidatorInterface */
    protected $validator;

    /** @var NormalizerInterface */
    protected $violationNormalizer;

    /**
     * CreateAction constructor.
     *
     * @param ActionFactory       $actionFactory
     * @param ActionEventManager  $eventManager
     * @param ManagerRegistry     $managerRegistry
     * @param ValidatorInterface  $validator
     * @param NormalizerInterface $violationNormalizer
     */
    public function __construct(
        RequestStack $requestStack,
        RouterInterface $router,
        ActionFactory $actionFactory,
        ActionEventManager $eventManager,
        ManagerRegistry $managerRegistry,
        ValidatorInterface $validator,
        NormalizerInterface $violationNormalizer
    ) {
        parent::__construct($actionFactory, $eventManager, $managerRegistry);
        $this->validator = $validator;
        $this->violationNormalizer = $violationNormalizer;
    }

    /**
     * {@inheritdoc}
     */
    protected function doExecute(Request $request): JsonResponse
    {
        $entity = $this->getManager()->create(
            $this->configuration->getEntityClass(),
            $this->getDecodedContent($request->getContent())
        );

        $errors = $this->validator->validate($entity);
        if (count($errors) > 0) {
            $normalizedViolations = [];
            foreach ($errors as $error) {
                $normalizedViolations[] = $this->violationNormalizer->normalize(
                    $error,
                    'internal_api'
                );
            }

            return new JsonResponse(['values' => $normalizedViolations], Response::HTTP_BAD_REQUEST);
        }

        $this->getManager()->save($entity);

        $response = [
            'status' => 1,
            'id'               => $entity->getId(),
            'customEntityName' => $this->configuration->getName(),
            'url'    => $this->router->generate(
                'myintegrationsconnector_activity_edit',
                ['code' => $entity->getCode()]
            )
        ];

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function getType(): string
    {
        return 'rest_create';
    }

    /**
     * Get the JSON decoded content. If the content is not a valid JSON, it throws an error 400.
     *
     * @param string $content content of a request to decode
     *
     * @throws BadRequestHttpException
     *
     * @return array
     */
    protected function getDecodedContent($content)
    {
        $decodedContent = json_decode($content, true);

        if (null === $decodedContent) {
            throw new BadRequestHttpException('Invalid json payload received');
        }

        return $decodedContent;
    }
}
