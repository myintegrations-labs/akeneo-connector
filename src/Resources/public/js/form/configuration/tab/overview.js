"use strict";

define(
    [
        'underscore',
        'oro/translator',
        'pim/form',
        'myintegrationsconnector/template/configuration/tab/overview',
    ],
    function(
        _,
        __,
        BaseForm,
        template,
    ) {
        return BaseForm.extend({
            isGroup: true,
            label: __('myintegrations-connector.overview'),
            template: _.template(template),
            code: 'myintegrations_connector_overview',
            // events: {
                // 'change .AknFormContainer-Mappings input': 'updateModel'
            // },

            /**
             * {@inheritdoc}
             */
            configure: function () {
                this.trigger('tab:register', {
                    code: this.code,
                    label: this.label
                });

                return BaseForm.prototype.configure.apply(this, arguments);
            },

            /**
             * {@inheritdoc}
             */
            render: function () {
                $('#container .AknButton--apply.save').hide();

                this.$el.html(this.template());

                // this.delegateEvents();

                return BaseForm.prototype.render.apply(this, arguments);
            },
        });
    }
);
