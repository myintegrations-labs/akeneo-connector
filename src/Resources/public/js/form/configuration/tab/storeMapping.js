"use strict";

define(
    [
        'underscore',
        'oro/translator',
        'pim/form',
        'myintegrationsconnector/template/configuration/tab/storeMapping',
    ],
    function(
        _,
        __,
        BaseForm,
        template,
    ) {
        return BaseForm.extend({
            isGroup: true,
            label: __('myintegrations-connector.store_mapping'),
            template: _.template(template),
            code: 'myintegrations_connector_store_mapping',
            events: {
                'change select': 'updateModel',
            },

            /**
             * {@inheritdoc}
             */
            configure: function () {
                this.trigger('tab:register', {
                    code: this.code,
                    label: this.label
                });

                return BaseForm.prototype.configure.apply(this, arguments);
            },

            /**
             * {@inheritdoc}
             */
            render: function () {
                $('#container .AknButton--apply.save').show();
                var storeViews = this.getFormData()['storeViews'];
                try {
                    if(typeof(storeViews) !== 'object') {
                        var storeViews = JSON.parse(storeViews);
                    }
                } catch(e) {
                    var storeViews = {};
                }

                this.$el.html(this.template({
                    locales: this.getFormData()['locales'],
                    storeViews: storeViews,
                    storeMapping: this.getFormData()['storeMapping'],
                }));

                this.delegateEvents();

                return BaseForm.prototype.render.apply(this, arguments);
            },

            /**
             * Update model after value change
             *
             * @param {Event} event
             */
            updateModel: function (event) {
                var data = this.getFormData();
                if(!data['storeMapping'])
                    data['storeMapping'] = {};

                data['storeMapping'][$(event.target).attr('name')] = event.target.value;
                this.setData(data);
            },
        });
    }
);
