"use strict";

define(
    [
        'underscore',
        'oro/translator',
        'pim/form',
        'myintegrationsconnector/template/configuration/tab/mapping',
        'pim/router',
        'oro/loading-mask',
        'pim/fetcher-registry',
        'pim/user-context',
    ],
    function(
        _,
        __,
        BaseForm,
        template,
        router,
        LoadingMask,
        FetcherRegistry,
        UserContext
    ) {
        return BaseForm.extend({
            isGroup: true,
            label: __('myintegrations-connector.attribute_mapping'),
            template: _.template(template),
            code: 'myintegrations_connector_mapping',
            events: {
                'change .AknFormContainer-Mappings input': 'updateModel',
                'change .AknFormContainer-Mappings select': 'updateModel'
            },
            fields: null,
            attributes: null,
            /**
             * {@inheritdoc}
             */
            configure: function () {
                this.trigger('tab:register', {
                    code: this.code,
                    label: this.label
                });

                return BaseForm.prototype.configure.apply(this, arguments);
            },

            /**
             * {@inheritdoc}
             */
            render: function () {
                var loadingMask = new LoadingMask();
                loadingMask.render().$el.appendTo(this.getRoot().$el).show();

                var fields;
                var attributes;
                if(this.fields && this.attributes) {
                    fields = this.fields;
                    attributes = this.attributes;
                } else {
                    fields = FetcherRegistry.getFetcher('magento-fields').fetchAll();
                    attributes = FetcherRegistry.getFetcher('attribute').search({options: {'page': 1, 'limit': 10000 } });
                }
                var self = this;
                Promise.all([fields, attributes]).then(function(values) {
                    $('#container .AknButton--apply.save').show();
                    self.fields = values[0];
                    self.attributes = values[1];

                    self.$el.html(self.template({
                        fields: self.fields,
                        attributes: self.attributes,
                        mapping: self.getFormData()['mapping'],
                        currentLocale: UserContext.get('uiLocale'),
                    }));

                    self.$('*[data-toggle="tooltip"]').tooltip();
                    loadingMask.hide().$el.remove();
                });

                this.delegateEvents();

                return BaseForm.prototype.render.apply(this, arguments);
            },

            /**
             * Update model after value change
             *
             * @param {Event} event
             */
            updateModel: function (event) {
                var data = this.getFormData();
                if(!data['mapping'])
                    data['mapping'] = {};

                data['mapping'][$(event.target).attr('name')] = event.target.value;
                this.setData(data);
                console.log(data);
            },
        });
    }
);
