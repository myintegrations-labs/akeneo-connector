'use strict';

define([
        'underscore',
        'pim/remover/base',
        'routing'
    ], function (
        _,
        BaseRemover,
        Routing
    ) {
        return _.extend({}, BaseRemover, {
            /**
             * {@inheritdoc}
             */
            getUrl: function (code) {
                console.log(__moduleConfig);
                return Routing.generate(__moduleConfig.url, {code: code});
            }
        });
    }
);
