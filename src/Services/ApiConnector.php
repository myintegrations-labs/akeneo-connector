<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Services;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use MyIntegrations\Bundle\ConnectorBundle\Entity;
use MyIntegrations\Bundle\ConnectorBundle\Component\Validator\ValidCredentials;
use Oro\Bundle\ConfigBundle\Entity\ConfigValue;
use MyIntegrations\Bundle\ConnectorBundle\Component\OAuthClient;

class ApiConnector
{
    const SECTION = 'magento2_connector';

    const SECTION_STORE_MAPPING = 'magento2_store_mapping';

    const SECTION_ATTRIBUTE_MAPPING = 'magento2_attribute_mapping';

    const SECTION_STORE_SETTING = 'magento2_store_';

    private $requiredKeys = ['hostName', 'consumerKey', 'consumerSecret', 'authToken', 'authSecret'];

    private $em;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }

    public function getCredentials()
    {
        $repo = $this->em->getRepository('OroConfigBundle:ConfigValue');
        $credentials = $repo->findBy([
            'section' => self::SECTION
        ]);

        $result = $this->indexValuesByName($credentials);

        /* storeviews */
        $storeMapping = $repo->findBy([
            'section' => self::SECTION_STORE_MAPPING
        ]);
        $result['storeMapping'] = $this->indexValuesByName($storeMapping);

        return $result;
    }

    public function getAttributeMappings()
    {
        $repo = $this->em->getRepository('OroConfigBundle:ConfigValue');
        $attrMappings = $repo->findBy([
            'section' => self::SECTION_ATTRIBUTE_MAPPING
        ]);

        return $this->indexValuesByName($attrMappings);
    }

    public function saveAttributeMappings($attributeData)
    {
        $repo = $this->em->getRepository('OroConfigBundle:ConfigValue');

        foreach($attributeData as $mCode => $aCode) {
            $mCode = strip_tags($mCode);
            $aCode = strip_tags($aCode);

            $attribute = $repo->findOneBy([
                'name' => $mCode,
                'section' => self::SECTION_ATTRIBUTE_MAPPING
            ]);
            if($attribute) {
                $attribute->setValue($aCode);
                $this->em->persist($attribute);
            } else {
                $attribute = new ConfigValue();
                $attribute->setSection(self::SECTION_ATTRIBUTE_MAPPING);
                $attribute->setName($mCode);
                $attribute->setValue($aCode);
                $this->em->persist($attribute);
            }
        }
        $this->em->flush();
    }

    public function checkCredentialAndGetStoreViews($params)
    {
        if(is_array($params) && $this->array_keys_exists($this->requiredKeys, $params) ) {
            return $this->fetchStoreApi($params);
        }
    }

    public function saveOtherSettings($params)
    {
        if(is_array($params) && $this->array_keys_exists($this->requiredKeys, $params) ) {
            $configs = json_decode($this->fetchStoreApi($params, $params['hostName'] . '/rest/V1/store/storeConfigs?'), true);
            $setting = [];
            foreach($configs as $config) {
                $setting[$config['code']] = [];
                foreach(['locale', 'base_currency_code', 'weight_unit'] as $property) {
                    if(isset($config[$property])) {
                        $setting[$config['code']][$property] = $config[$property];
                    }
                }
            }
            $repo = $this->em->getRepository('OroConfigBundle:ConfigValue');

            $configValue = $repo->findOneBy([
                'section' => self::SECTION_STORE_SETTING,
                'name' => 'settings',
            ]);
            if($configValue) {
                $configValue->setValue(json_encode($setting));
            } else {
                $configValue = new ConfigValue();
                $configValue->setValue(json_encode($setting));
                $configValue->setSection(self::SECTION_STORE_SETTING);
                $configValue->setName('settings');
            }
            $this->em->persist($configValue);

            $this->em->flush();
        }
    }

    public function getOtherSettings()
    {
        static $otherSettings;

        if(empty($otherSettings)) {
            $repo = $this->em->getRepository('OroConfigBundle:ConfigValue');

            $configValue = $repo->findOneBy([
                'section' => self::SECTION_STORE_SETTING,
                'name' => 'settings',
            ]);
            $otherSettings = $configValue->getValue() ? json_decode($configValue->getValue(), true) : null;
        }

        return $otherSettings;
    }


    private function fetchStoreApi($params, $url = null)
    {
        try {
            $oauthClient = new OAuthClient($params['authToken']);
            if(empty($url)) {
                $url = $params['hostName'] . '/rest/V1/store/storeViews?';
            }

            $oauthClient->fetch($url, [], 'GET', ['Content-Type' => 'application/json', 'Accept' => 'application/json'] );

            $results = $oauthClient->getLastResponse();
        } catch(\Exception $e) {
            $results = null;
        }
        return $results;
    }

    private function array_keys_exists(array $keys, array $arr )
    {
        $flag = true;
        foreach($keys as $key) {
            if(!array_key_exists($key, $arr)) {
                $flag = false;
                break;
            }
        }

        return $flag;
    }

    private function indexValuesByName($values)
    {
        $result = [];
        foreach($values as $value) {
            $result[$value->getName()] = $value->getValue();
        }
        return $result;
    }

}
