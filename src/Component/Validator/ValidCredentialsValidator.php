<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Component\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use MyIntegrations\Bundle\ConnectorBundle\Component\Validator\ValidCredentials;
use MyIntegrations\Bundle\ConnectorBundle\Component\OAuthClient;

class ValidCredentialsValidator extends ConstraintValidator
{

    private $requiredKeys = ['hostName', 'consumerKey', 'consumerSecret', 'authToken', 'authSecret'];

    /**
     * {@inheritdoc}
     */
    public function validate($singleValue, Constraint $constraint= null)
    {
        $value =  $this->context->getRoot()->getData();
        if(!empty($value['filters'])) {
            unset($value['filters']);
        }

        if (!$constraint instanceof ValidCredentials) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\CredentialValidator');
        }
        if(is_array($value) && $this->array_keys_exists($this->requiredKeys, $value) ) {
            $success = $this->fetchStoreApi($value);
        }

        if(empty($success)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode('inv-credential-err')
                ->addViolation();
        }
        unset($success);
        return true;
    }

    private function array_keys_exists(array $keys, array $arr )
    {
        $flag = true;
        foreach($keys as $key) {
            if(!array_key_exists($key, $arr)) {
                $flag = false;
                break;
            }
        }

        return $flag;
    }


    private function fetchStoreApi($params)
    {
        try {
            $oauthClient = new OAuthClient($params['authToken']);
            $url = $params['hostName'] . '/rest/V1/store/storeViews';

            $results = $oauthClient->fetch($url, [], 'GET', ['Content-Type' => 'application/json', 'Accept' => 'application/json'] );
        } catch(\Exception $e) {
            $results = null;
        }
        return $results;
    }
}
