<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Component\Normalizer;

use Doctrine\Common\Collections\ArrayCollection;
use Pim\Bundle\CatalogBundle\Filter\CollectionFilterInterface;
use Pim\Component\Catalog\Model\ProductInterface;
use Pim\Component\Catalog\Model\ValueCollectionInterface;
use Pim\Component\Catalog\Model\VariantProductInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;
use Pim\Component\Catalog\Model\ProductModel;

/**
 * Transform the properties of a product object (fields and product values)
 * to a standardized array
 */
class PropertiesNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    const FIELD_IDENTIFIER = 'identifier';
    const FIELD_FAMILY = 'family';
    const FIELD_PARENT = 'parent';
    const FIELD_PARENT_SKU = 'parent-sku';
    const FIELD_ENABLED = 'status';
    const FIELD_VALUES = 'values';
    const FIELD_CREATED = 'created_at';
    const FIELD_UPDATED = 'updated_at';
    const FIELD_VISIBILITY = 'visibility';
    const FIELD_GROUPS = 'groups';
    const FIELD_CATEGORIES = 'categories';
    const FIELD_AXIS = 'axes';
    const FIELD_VARIANTS = 'variants';
    const VARIANT_ATTRIBUTES = 'allVariantAttributes';
    const FIELD_META_DATA = 'metadata';
    const FIELD_MAGENTO_PRODUCT_TYPE = 'type_id';
    const ATTRIBUTE_AS_IMAGE = 'main-image';

    const CONFIGURABLE_TYPE = 'configurable';
    const SIMPLE_TYPE = 'simple';
    const VARIANT_TYPE = 'variant';

    const VISIBILITY_ALL = 4;
    const VISIBILITY_CATALOG = 3;
    const VISIBILITY_SEARCH = 1;
    const VISIBILITY_NOT_INDIVIDUALLY = 1;

    /** @var CollectionFilterInterface */
    private $filter;

    /**
     * @param CollectionFilterInterface $filter The collection filter
     */
    public function __construct(CollectionFilterInterface $filter)
    {
        $this->filter = $filter;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($product, $format = null, array $context = [])
    {
        if (!$this->serializer instanceof NormalizerInterface) {
            throw new \LogicException('Serializer must be a normalizer');
        }

        $context = array_merge(['filter_types' => ['pim.transform.product_value.structured']], $context);
        $data = [];

        $data[self::FIELD_META_DATA] = [];
        $data[self::FIELD_META_DATA][self::FIELD_FAMILY] = $product->getFamily() ? $product->getFamily()->getCode() : null;
        $data[self::FIELD_META_DATA][self::ATTRIBUTE_AS_IMAGE] = $product->getFamily()->getAttributeAsImage() ? $product->getFamily()->getAttributeAsImage()->getCode() : null ;

        $data[self::FIELD_META_DATA][self::FIELD_CATEGORIES] = $product->getCategoryCodes();
        // $data[self::FIELD_GROUPS] = $product->getGroupCodes();
        if(!$product instanceOf ProductModel) {
            $data[self::FIELD_META_DATA][self::FIELD_IDENTIFIER] = $product->getIdentifier();
            $data[self::FIELD_ENABLED] = (int) $product->isEnabled();
        }

        if ($product instanceof VariantProductInterface && null !== $product->getParent()) {
            $data[self::FIELD_AXIS] = $this->getVariantAxes($product);
            $data[self::VARIANT_ATTRIBUTES] = $data[self::FIELD_PARENT][self::VARIANT_ATTRIBUTES] = $this->getAllVariantAttributes($product);
            $data[self::FIELD_VARIANTS] = $product->getFamilyVariant()->getCode();

            $parent = $this->getMainParent($product);
            if($parent && $parent instanceOf ProductModel) {
                $data[self::FIELD_PARENT] = $this->normalize($parent, $format, $context);
                $data[self::FIELD_PARENT]['sku'] = $parent->getCode();
            }
            $data[self::FIELD_VISIBILITY]           = self::VISIBILITY_NOT_INDIVIDUALLY;
            $data[self::FIELD_MAGENTO_PRODUCT_TYPE] = self::VARIANT_TYPE;
        } else if($product instanceof ProductModel) {
            $data[self::FIELD_ENABLED] = 1;
            $data[self::FIELD_VISIBILITY]           = self::VISIBILITY_ALL;
            $data[self::FIELD_MAGENTO_PRODUCT_TYPE] = self::CONFIGURABLE_TYPE;
        } else {
            $data[self::FIELD_VISIBILITY]           = self::VISIBILITY_ALL;
            $data[self::FIELD_META_DATA][self::FIELD_PARENT] = null;
            $data[self::FIELD_MAGENTO_PRODUCT_TYPE] = self::SIMPLE_TYPE;
        }

        $normalizedProductValues = $this->normalizeValues($product->getValues(), $format, $context);

        $data[self::FIELD_UPDATED] = $this->serializer->normalize($product->getUpdated(), $format);
        $data[self::FIELD_VALUES] = $normalizedProductValues;
        // $data[self::FIELD_CREATED] = $this->serializer->normalize($product->getCreated(), $format);

        // if( !empty($normalizedProductValues['image']) ) {
        //     $images = array_map([$this, 'getDataIndex'], $normalizedProductValues['image']);
        //     if(!empty($images)) {
        //         $data['media_gallery_entries'] = $images;
        //     }
        // } else if( !empty($normalizedProductValues['picture']) ) {
        //     $images = array_map([$this, 'getDataIndex'], $normalizedProductValues['picture']);
        //     if(!empty($images)) {
        //         $data['media_gallery_entries'] = $images;
        //     }
        // }

        // variation_image, notice
        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ProductInterface && 'standard' === $format;
    }


    protected function getDataIndex($array)
    {
        return $array['data'] ?? null;
    }

    protected function getMainParent($product)
    {
        $maxLoop = 10;
        $self = $product;
        while($self->getParent() instanceOf ProductModel && $maxLoop) {
            $self = $self->getParent();
            $maxLoop--;
        }

        return $self !== $product ? $self : null;
    }

    protected function getVariantAxes($product)
    {
        $result = [];
        $varAttributeSets = $product->getFamilyVariant()->getVariantAttributeSets();
        foreach($varAttributeSets as $attrSet) {
            $axises = $attrSet->getAxes();
            foreach($axises as $axis) {
                $result[] = $axis->getCode();
            }
        }

        return $result;
    }

    protected function getAllVariantAttributes($product)
    {
        $result = [];
        $varAttributeSets = $product->getFamilyVariant()->getVariantAttributeSets();
        foreach($varAttributeSets as $attrSet) {
            $attributes = $attrSet->getAttributes();
            foreach($attributes as $attribute) {
                $result[] = $attribute->getCode();
            }
        }

        return $result;
    }

    /**
     * Normalize the values of the product
     *
     * @param ValueCollectionInterface $values
     * @param string                   $format
     * @param array                    $context
     *
     * @return ArrayCollection
     */
    private function normalizeValues(ValueCollectionInterface $values, $format, array $context = [])
    {
        foreach ($context['filter_types'] as $filterType) {
            $values = $this->filter->filterCollection($values, $filterType, $context);
        }
        $data = $this->serializer->normalize($values, $format, $context);

        return $data;
    }

}
