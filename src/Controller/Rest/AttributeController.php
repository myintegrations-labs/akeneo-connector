<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AttributeController extends Controller
{
    public function indexAction()
    {
        $saver = $this->get('my_integrations_connector.updater.event');

        $newEvent = new Event();
        $newEvent->setName('nieuwe kleur');
        $newEvent->setData(time());
        $newEvent->setCode(time());

        $saver->save($newEvent);
//        $saver->flush();


        return $this->render('MyIntegrationsConnectorBundle:Default:index.html.twig');
    }
}
