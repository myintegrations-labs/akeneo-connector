<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Controller\Rest;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Form\FormError;
use Oro\Bundle\ConfigBundle\Entity\ConfigValue;
use Akeneo\Component\Batch\Model\JobInstance;

use MyIntegrations\Bundle\ConnectorBundle\Entity;
use MyIntegrations\Bundle\ConnectorBundle\Entity\Event;
use MyIntegrations\Bundle\ConnectorBundle\Component\Validator\ValidCredentials;

class ConfigurationController extends Controller
{
    const SECTION = 'myintegrations-connector';

    const SECTION_ATTRIBUTE_MAPPING = 'myintegrations-connector_store_mapping';
    const QUICK_EXPORT_CODE = 'myintegrations-connector_product_quick_export';

    /**
     * Get the current configuration
     *
     *
     * @return JsonResponse
     */
    public function getAction()
    {
        $data = $this->get('my_integrations_connector.service.api_connector')->getCredentials();
        $data['mapping'] = $this->get('my_integrations_connector.service.api_connector')->getAttributeMappings();
        $data['locales'] = $this->get('pim_catalog.repository.locale')->getActivatedLocalesQB()->getQuery()->getArrayResult();

        return new JsonResponse($data);
    }

    /**
     * Set the current configuration
     *
     * @return JsonResponse
     */
    public function postAction(Request $request)
    {
        $data  = [];
        $request->request->remove('locales');
        $params = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $connectorService = $this->get('my_integrations_connector.service.api_connector');

        switch($request->attributes->get('tab')) {
            case 'mapping':
                $attributeData = !empty($params['mapping']) ? $params['mapping'] : null;
                if($attributeData) {
                    $connectorService->saveAttributeMappings($attributeData);
                }
                return new JsonResponse([]);
                break;

            case 'storeMapping':
                $mapData = !empty($params['storeMapping']) ? $params['storeMapping'] : null;
                if($mapData) {
                    $repo = $em->getRepository('OroConfigBundle:ConfigValue');

                    foreach($mapData as $locale => $code) {
                        $configValue = $repo->findOneBy([
                            'section' => self::SECTION_ATTRIBUTE_MAPPING, 'name' => $locale
                        ]);

                        if($configValue) {
                            $configValue->setValue($code);
                            $em->persist($configValue);
                        } else {
                            $configValue = new ConfigValue();
                            $configValue->setSection(self::SECTION_ATTRIBUTE_MAPPING);
                            $configValue->setName($locale);
                            $configValue->setValue($code);
                            $em->persist($configValue);
                        }
                        $em->flush();
                    }
                }
                return new JsonResponse([]);
                break;

            case 'credential':
            default:
                $form = $this->getConfigForm();
                $form->submit($params);
                $form->handleRequest($request);
                $storeViews = $connectorService->checkCredentialAndGetStoreViews($params);
                if(empty($storeViews)) {
                    $form->get('authToken')->addError(new FormError($this->get('translator')->trans('Invalid Credentials.') ));
                } else {
                    $params['storeViews'] = $storeViews;
                }

                if ($form->isSubmitted() && $form->isValid()) {
                    $this->checkAndSaveQuickJob();

                    $repo = $em->getRepository('OroConfigBundle:ConfigValue');
                    foreach($params as $key => $value) {
                        if(in_array($key, array_keys($this->getCredentialWithContraints())) || $key == 'storeViews' ) {
                            if(!is_array($value)) {
                                $value = strip_tags($value);

                                $configValue = $repo->findOneBy([
                                    'section' => self::SECTION, 'name' => $key
                                ]);

                                if($configValue) {
                                    $configValue->setValue($value);
                                    $em->persist($configValue);
                                } else {
                                    $configValue = new ConfigValue();
                                    $configValue->setSection(self::SECTION);
                                    $configValue->setName($key);
                                    $configValue->setValue($value);
                                    $em->persist($configValue);
                                }
                                $em->flush();
                            }
                        }
                    }
                    $connectorService->saveOtherSettings($params);
                } else {
                    return new JsonResponse($this->getFormErrors($form), RESPONSE::HTTP_BAD_REQUEST);
                }
        }

        return $this->getAction();
    }

    public function getDataAction()
    {
        $saver = $this->get('doctrine.orm.entity_manager');

        $newEvent = new Event();
        $newEvent->setName('nieuwe kleur');
        $newEvent->setData(time());
        $newEvent->setCode(time());

        $saver->persist($newEvent);
        $saver->flush();
        return new JsonResponse($newEvent);
        return new JsonResponse($this->mappingFields);
    }

    protected function checkAndSaveQuickJob()
    {
        $jobInstance = $this->get('pim_enrich.repository.job_instance')->findOneBy(['code' => self::QUICK_EXPORT_CODE]);

        if(!$jobInstance) {
            $em = $this->getDoctrine()->getManager();
            $jobInstance = new JobInstance();
            $jobInstance->setCode(self::QUICK_EXPORT_CODE);
            $jobInstance->setJobName('myintegrations-connector_quick_export');
            $jobInstance->setLabel('Magento 2 product quick export');
            $jobInstance->setConnector('Magento 2 Export Connector');
            $jobInstance->setType('quick_export');
            $em->persist($jobInstance);
            $em->flush();
        }
    }

    private function getConfigForm()
    {
        $form = $this->createFormBuilder(null, [
            'allow_extra_fields' => true,
            'csrf_protection' => false
        ]);

        foreach($this->getCredentialWithContraints() as $field => $constraint) {
            $form->add($field, null, [
                'constraints' => [
                    $constraint
                ]
            ]);
        }

        return $form->getForm();
    }

    private function getFormErrors($form)
    {
        $errorContext = [];
        foreach ($form->getErrors(true) as $key => $error) {
            $errorContext[$error->getOrigin()->getName()] = $error->getMessage();
        }

        return $errorContext;
    }

    private function getCredentialWithContraints()
    {
        return [
            'hostName' => new Url(),
            'consumerKey' => new NotBlank(),
            'consumerSecret' => new NotBlank(),
            'authToken' => new NotBlank(),
            'authSecret' => new NotBlank(),
        ];
    }

    private $mappingFields = [
        [
            'name' => 'name',
            'label' => 'myintegrations-connector.attribute.name',
            'placeholder' => '',
            'types' => [
                'pim_catalog_text',
            ],
            'tooltip' => 'supported attributes types: text',
        ],
        [
            'name' => 'weight',
            'label' => 'myintegrations-connector.attribute.weight',
            'placeholder' => '',
            'types' => [
                'pim_catalog_metric',
            ],
            'tooltip' => 'supported attributes types: metric',
        ],
        [
            'name' => 'price',
            'label' => 'myintegrations-connector.attribute.price',
            'placeholder' => '',
            'types' => [
                'pim_catalog_price_collection',
            ],
            'tooltip' => 'supported attributes types: price',
        ],
        [
            'name' => 'description',
            'label' => 'myintegrations-connector.attribute.description',
            'placeholder' => '',
            'types' => [
                'pim_catalog_textarea',
            ],
            'tooltip' => 'supported attributes types: textarea',
        ],
        [
            'name' => 'short_description',
            'label' => 'myintegrations-connector.attribute.short_description',
            'placeholder' => '',
            'types' => [
                'pim_catalog_textarea',
            ],
            'tooltip' => 'supported attributes types: textarea',
        ],
        [
            'name' => 'quantity',
            'label' => 'myintegrations-connector.attribute.quantity',
            'placeholder' => '',
            'types' => [
                'pim_catalog_number',
            ],
            'tooltip' => 'supported attributes types: number',
        ],
        // [
        //     'name' => 'visibility',
        //     'label' => 'myintegrations-connector.attribute.visibility',
        //     'placeholder' => 'by default: visible on both search and catalog',
        //     'types' => [
        //     'pim_catalog_boolean',
        //  ],
        // ],
        [
            'name' => 'meta_title',
            'label' => 'myintegrations-connector.attribute.meta_title',
            'placeholder' => '',
            'types' => [
                'pim_catalog_text',
            ],
            'tooltip' => 'supported attributes types: text',
        ],
        [
            'name' => 'meta_keyword',
            'label' => 'myintegrations-connector.attribute.meta_keyword',
            'placeholder' => '',
            'types' => [
                'pim_catalog_text',
            ],
            'tooltip' => 'supported attributes types: text',
        ],
        [
            'name' => 'meta_description',
            'label' => 'myintegrations-connector.attribute.meta_description',
            'placeholder' => '',
            'types' => [
                'pim_catalog_text',
            ],
            'tooltip' => 'supported attributes types: text',
        ],
        [
            'name' => 'url_key',
            'label' => 'myintegrations-connector.attribute.url_key',
            'placeholder' => '',
            'types' => [
                'pim_catalog_text',
            ],
            'tooltip' => 'supported attributes types: text',
        ],
        [
            'name' => 'news_from_date',
            'label' => 'myintegrations-connector.attribute.new_from',
            'placeholder' => '',
            'types' => [
                'pim_catalog_date',
            ],
            'tooltip' => 'supported attributes types: date',
        ],
        [
            'name' => 'news_to_date',
            'label' => 'myintegrations-connector.attribute.new_till',
            'placeholder' => '',
            'types' => [
                'pim_catalog_date',
            ],
            'tooltip' => 'supported attributes types: date',
        ],
        [
            'name' => 'special_price',
            'label' => 'myintegrations-connector.attribute.special_price',
            'placeholder' => '',
            'types' => [
                'pim_catalog_price_collection',
            ],
            'tooltip' => 'supported attributes types: price',
        ],
        [
            'name' => 'special_price_from',
            'label' => 'myintegrations-connector.attribute.special_price_from',
            'placeholder' => '',
            'types' => [
                'pim_catalog_date',
            ],
            'tooltip' => 'supported attributes types: date',
        ],
        [
            'name' => 'special_price_till',
            'label' => 'myintegrations-connector.attribute.special_price_till',
            'placeholder' => '',
            'types' => [
                'pim_catalog_date',
            ],
            'tooltip' => 'supported attributes types: date',
        ],
        // [
        //     'name' => 'purchase_limit',
        //     'label' => 'myintegrations-connector.attribute.purchase_limit',
        //     'placeholder' => '',
        //     'types' => [
        // 'pim_catalog_number',
        // ],
        // ],
    ];
}

