<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Controller;

use MyIntegrations\Bundle\ConnectorBundle\Configuration\Registry;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ActivityController extends Controller
{
    /** @var ConfigurationRegistry */
    protected $configurations;

    public function __construct(Registry $configurations)
    {
        $this->configurations = $configurations;
    }

    /**
     * Return the list of registred references data
     *
     * @return JsonResponse
     */
    public function listAction()
    {
        $referenceDataNames = $this->configurations->getNames();

        return new JsonResponse(array_combine($referenceDataNames, $referenceDataNames));
    }
}
