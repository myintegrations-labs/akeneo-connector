<?php

namespace MyIntegrations\Bundle\ConnectorBundle\EventListener;

use Symfony\Component\EventDispatcher\GenericEvent;
use Pim\Component\Catalog\Model\CategoryInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

class CategoryModificationListener
{
    private $saver;

    public function __construct($saver)
    {
        $this->saver = $saver;
    }

    public function onPostSave(GenericEvent $event)
    {
        $subject = $event->getSubject();

        if (!$subject instanceof CategoryInterface) {
            // don't do anything if it's not a product
            return;
        }


    }

    public function prePersist(LifecycleEventArgs $event)
    {
        $subject = $event->getEntity();

        if (!$subject instanceof CategoryInterface) {
            // don't do anything if it's not a product
            return;
        }


    }

    public function postUpdate(GenericEvent $event)
    {
        $subject = $event->getSubject();

        if (!$subject instanceof CategoryInterface) {
            // don't do anything if it's not a product
            return;
        }


    }
}
