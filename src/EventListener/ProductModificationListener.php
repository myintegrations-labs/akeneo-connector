<?php

namespace MyIntegrations\Bundle\ConnectorBundle\EventListener;

use MyIntegrations\Bundle\ConnectorBundle\Entity\Event;
use Symfony\Component\EventDispatcher\GenericEvent;
use Pim\Bundle\CatalogBundle\Model\ProductInterface;
use Pim\Component\ReferenceData\Model\ReferenceDataInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

class ProductModificationListener
{
    private $saver;

    public function __construct($saver)
    {
        $this->saver = $saver;
    }

    public function onPostSave(GenericEvent $event)
    {
        $subject = $event->getSubject();

        if (!$subject instanceof ProductInterface) {
            // don't do anything if it's not a product
            return;
        }


        $newEvent = new Event();
        $newEvent->setData(serialize($subject));


        $this->saver->save($newEvent);
    }

    public function prePersist(LifecycleEventArgs $event)
    {
        $subject = $event->getEntity();

        if (!$subject instanceof ProductInterface) {
            // don't do anything if it's not a product
            return;
        }


        $newEvent = new Event();
        $newEvent->setData(serialize($subject));


        $this->saver->save($newEvent);
    }
}
