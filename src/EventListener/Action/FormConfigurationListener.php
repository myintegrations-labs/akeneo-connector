<?php

namespace MyIntegrations\Bundle\ConnectorBundle\EventListener\Action;

use MyIntegrations\Bundle\ConnectorBundle\Action\AbstractFormAction;
use MyIntegrations\Bundle\ConnectorBundle\Event\ActionEvents;
use MyIntegrations\Bundle\ConnectorBundle\Event\ConfigurationEvent;
use MyIntegrations\Bundle\ConnectorBundle\Event\ConfigurationEvents;
use MyIntegrations\Bundle\ConnectorBundle\Event\ConfigureActionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Form listener for actions
 *
 * @author    Antoine Guigan <antoine@akeneo.com>
 * @copyright 2014 Akeneo SAS (http://www.akeneo.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class FormConfigurationListener implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            ConfigurationEvents::CONFIGURE => 'setConfigurationOptions',
            ActionEvents::CONFIGURE        => 'setActionOptions'
        ];
    }

    /**
     * Adds options to the actions
     *
     * @param ConfigureEvent $event
     */
    public function setConfigurationOptions(ConfigurationEvent $event)
    {
        $event->getOptionsResolver()->setDefined(
            [
                'form_type',
                'form_options',
                'form_template'
            ]
        );
    }

    /**
     * Adds options to the actions
     *
     * @param ConfigureActionEvent $event
     *
     * @return null
     */
    public function setActionOptions(ConfigureActionEvent $event)
    {
        if (!($event->getAction() instanceof AbstractFormAction)) {
            return;
        }

        $confOptions = $event->getAction()->getConfiguration()->getOptions();
        $resolver = $event->getOptionsResolver();
        if (isset($confOptions['form_type'])) {
            $resolver->setDefaults(['form_type' => $confOptions['form_type']]);
        }
        if (isset($confOptions['form_options'])) {
            $resolver->setDefaults(['form_options' => $confOptions['form_options']]);
        }
        if (isset($confOptions['form_template'])) {
            $resolver->setDefaults(['template' => $confOptions['form_template']]);
        }
    }
}
