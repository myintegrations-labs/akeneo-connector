<?php

namespace MyIntegrations\Bundle\ConnectorBundle\EventListener;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Doctrine\Common\Collections\ArrayCollection;
use MyIntegrations\Bundle\ConnectorBundle\Normalizer\EntityNormalizer;
use Pim\Component\Catalog\Model\ProductInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use MyIntegrations\Bundle\ConnectorBundle\Entity\Event;
use MyIntegrations\Bundle\ConnectorBundle\Entity\Color;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;

class ColorSubscriber implements EventSubscriber
{
    /**
     * @var bool
     */
    protected $needsFlush = false;

    /**
     * ColorSubscriber constructor.
     * @param $saver
     * @param Serializer $serializer
     */
    public function __construct(
        $saver,
        $serializer
    ) {
        $this->saver = $saver;
        $this->serializer = $serializer;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return array(
            'postpersist',
            'postUpdate',
            'onFlush',
            'postFlush',
        );
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
//        $this->execute($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args) {

        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if(!$entity instanceof Event) {
            $event = $this->addEvent($args->getEntity(), $args->getEntityManager());
            $em->persist($event);
            $this->needsFlush = true;
        }
    }

    /**
     * @param OnFlushEventArgs $args
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     * @throws \ReflectionException
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $em  = $args->getEntityManager();
        $uow  = $em->getUnitOfWork();
        $cmf = $em->getMetadataFactory();

        foreach ($uow->getScheduledEntityInsertions() AS $entity)
        {
            $meta = $cmf->getMetadataFor(get_class($entity));

            $this->updateTagged($em, $entity);
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity)
        {
            $meta = $cmf->getMetadataFor(get_class($entity));

            $this->updateTagged($em, $entity);
        }
    }

    /**
     * @param $em
     * @param $entity
     */
    public function updateTagged($em, $entity)
    {
        $event = $this->addEvent($entity, $em);

        $uow      = $em->getUnitOfWork();
        $cmf      = $em->getMetadataFactory();
        $meta     = $cmf->getMetadataFor(get_class($event));

        $em->persist($event);

        $uow->computeChangeSet($meta, $event);
    }

    /**
     * @param PostFlushEventArgs $eventArgs
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postFlush(PostFlushEventArgs $eventArgs)
    {
        if ($this->needsFlush) {
            $this->needsFlush = false;
            $eventArgs->getEntityManager()->flush();
        }
    }

    /**
     * @param $entity
     * @param $em
     * @return Event
     */
    protected function addEvent($entity, $em)
    {
        $event = new Event();
        $event->setName(time());
        $event->setEntity($this->convertToString($entity));
        $event->setData($this->convertToJson($entity, $em));
        $event->setCode(uniqid());

        return $event;
    }

    /**
     * @param $object
     * @return string
     */
    protected function convertToString($object)
    {
        return strtolower(join('', array_slice(explode('\\', get_class($object)), -1)));
    }

    /**
     * @param $object
     * @return mixed
     */
    protected function convertToJson($object, $objectManager)
    {
        $hydrator = new DoctrineHydrator($objectManager);

        return $hydrator->extract($object);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function execute(LifecycleEventArgs $args)
    {
        $objectManager = $args->getObjectManager();
        $entity = $args->getEntity();

        // only act on some "Product" entity
//        if ($entity instanceof Color || $entity instanceof ProductInterface) {
            $newEvent = new Event();
            $newEvent->setName(time());
            $newEvent->setEntity($this->convertToString($entity));
            $newEvent->setData($this->convertToJson($entity, $objectManager));
            $newEvent->setCode(uniqid());

            $entityManager->persist($newEvent);
            $entityManager->flush();
//        }


    }
}
