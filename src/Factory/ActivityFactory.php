<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Factory;

use Akeneo\Component\StorageUtils\Factory\SimpleFactoryInterface;
use Pim\Component\Catalog\Model\FamilyInterface;
use Pim\Component\Catalog\Factory\AttributeRequirementFactory;
use Pim\Component\Catalog\Repository\AttributeRepositoryInterface;
use Pim\Component\Catalog\Repository\ChannelRepositoryInterface;

class ActivityFactory implements SimpleFactoryInterface
{
    /** @var AttributeRequirementFactory */
    protected $factory;

    /** @var AttributeRepositoryInterface */
    protected $attributeRepository;

    /** @var string */
    protected $modelClass;

    /**
     * @param AttributeRequirementFactory  $factory
     * @param AttributeRepositoryInterface $attributeRepository
     * @param string                       $modelClass
     */
    public function __construct(
        AttributeRequirementFactory $factory,
        AttributeRepositoryInterface $attributeRepository,
        $modelClass
    ) {
        $this->factory = $factory;
        $this->attributeRepository = $attributeRepository;
        $this->modelClass = $modelClass;
    }

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        /** @var FamilyInterface $family */
        $model = new $this->modelClass();
        $identifier = $this->attributeRepository->getIdentifier();

        $model->addAttribute($identifier);
        $model->setAttributeAsLabel($identifier);

//        foreach ($this->getChannels() as $channel) {
//            $requirement = $this->factory->createAttributeRequirement($identifier, $channel, true);
//            $model->addAttributeRequirement($requirement);
//        }

        return $model;
    }
}
