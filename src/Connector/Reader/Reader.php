<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Connector\Reader;

use Akeneo\Component\Batch\Item\InitializableInterface;
use Akeneo\Component\Batch\Item\ItemReaderInterface;
use Akeneo\Component\Batch\Step\StepExecutionAwareInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Pim\Component\Connector\Reader\Database\AbstractReader;

class Reader extends AbstractReader implements
    ItemReaderInterface,
    InitializableInterface,
    StepExecutionAwareInterface
{
    /** @var ObjectRepository */
    protected $repository;

    /**
     * @param ObjectRepository $repository
     */
    public function __construct(ObjectRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    protected function getResults()
    {
        // $parameters = $this->stepExecution->getJobParameters();

        return new \ArrayIterator($this->repository->findAll());
    }
}
