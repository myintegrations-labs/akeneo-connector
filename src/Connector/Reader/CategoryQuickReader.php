<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Connector\Reader;

use Akeneo\Component\Batch\Item\InitializableInterface;
use Akeneo\Component\Batch\Item\ItemReaderInterface;
use Akeneo\Component\Batch\Step\StepExecutionAwareInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Pim\Component\Connector\Reader\Database\AbstractReader;
use Pim\Component\Catalog\Query\Filter\Operators;
use MyIntegrations\Bundle\ConnectorBundle\Traits\ChannelAwareTrait;

/**
 * attribute reader, basic
 */
class CategoryQuickReader extends AbstractReader implements
    ItemReaderInterface,
    InitializableInterface,
    StepExecutionAwareInterface
{
    use ChannelAwareTrait;

    /** @var ObjectRepository */
    protected $repository;

    /**
     * @param ObjectRepository $repository
     */
    public function __construct(ObjectRepository $repository, $channelRepo)
    {
        $this->repository = $repository;
        $this->channelRepo = $channelRepo;
    }

    /**
     * {@inheritdoc}
     */
    protected function getResults()
    {
        $filters = $this->stepExecution->getJobParameters()->get('filters');

        $rootCategoryId = $this->getDefaultCategoryTreeId($this->stepExecution->getJobParameters());

        $rootCategoryCode = $this->getDefaultCategoryTreeCode($this->stepExecution->getJobParameters());

        // $filteredCategories = $this->getCategoriesFromFilter($filters['data']);
        $filteredCategories = [ $rootCategoryCode ];
        if(count($filteredCategories) == 1 && $rootCategoryCode === reset($filteredCategories)) {
            if($rootCategoryId) {
                $categories = $this->repository->findBy(
                                [ 'root' => $rootCategoryId ],
                                [ 'root' => 'ASC', 'left' => 'ASC' ]
                            );
            } else {
                $categories = $this->repository->getOrderedAndSortedByTreeCategories();
            }

            foreach($categories as $key => $category) {
                if($rootCategoryId == $category->getId()) {
                    unset($categories[$key]);
                    break;
                }
            }
        } else {
            $categories = $this->repository->getCategoriesByCodes($filteredCategories);
            if($categories instanceof ArrayCollection) {
                $categories = $categories->toArray();
            }
        }

        return new \ArrayIterator($categories);
    }

    private function getCategoriesFromFilter($data)
    {
        $result = null;
        foreach($data as $key => $value) {
            if(!empty($value['field']) && 'categories' == $value['field']  ) {
                $result = $value['value'];
            }
        }
        return $result;
    }
}
