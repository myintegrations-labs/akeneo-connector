<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Connector\Reader;

use Akeneo\Component\Batch\Item\InitializableInterface;
use Akeneo\Component\Batch\Item\ItemReaderInterface;
use Akeneo\Component\Batch\Step\StepExecutionAwareInterface;
use Akeneo\Component\Classification\Repository\CategoryRepositoryInterface;
use Pim\Bundle\CatalogBundle\Doctrine\ORM\Repository\ChannelRepository;
use Pim\Component\Connector\Reader\Database\CategoryReader as PimCategoryReader;
use MyIntegrations\Bundle\ConnectorBundle\Traits\ChannelAwareTrait;
use Doctrine\Common\Collections\ArrayCollection;

class CategoryReader extends PimCategoryReader
{
    use ChannelAwareTrait;

    /**
     * @param CategoryRepositoryInterface $repository
     */
    public function __construct(CategoryRepositoryInterface $repository,ChannelRepository $channelRepo)
    {
        $this->repository = $repository;
        $this->channelRepo = $channelRepo;
    }

    /**
     * {@inheritdoc}
     */
    protected function getResults()
    {
        $filters = $this->stepExecution->getJobParameters()->get('filters');

        $rootCategoryId = $this->getDefaultCategoryTreeId($this->stepExecution->getJobParameters());
        $rootCategoryCode = $this->getDefaultCategoryTreeCode($this->stepExecution->getJobParameters());

        $filteredCategories = $this->getCategoriesFromFilter($filters['data']);

        if($this->isCategoryOnlyExport() || count($filteredCategories) == 1 && $rootCategoryCode === reset($filteredCategories)) {
            if($rootCategoryId) {
                $categories = $this->repository->findBy(
                                [ 'root' => $rootCategoryId ],
                                [ 'root' => 'ASC', 'left' => 'ASC' ]
                            );
            } else {
                $categories = $this->repository->getOrderedAndSortedByTreeCategories();
            }

            foreach($categories as $key => $category) {
                if($rootCategoryId == $category->getId()) {
                    unset($categories[$key]);
                    break;
                }
            }
        } else {
            $categories = $this->repository->getCategoriesByCodes($filteredCategories);
            if($categories instanceof ArrayCollection) {
                $categories = $categories->toArray();
            }
        }

        return new \ArrayIterator($categories);
    }

    private function getCategoriesFromFilter($data)
    {
        $result = null;
        foreach($data as $key => $value) {
            if(!empty($value['field']) && 'categories' == $value['field']  ) {
                $result = $value['value'];
            }
        }
        return $result;
    }

    private function isCategoryOnlyExport()
    {
        try {
            $flag = $this->stepExecution->getJobExecution()->getJobInstance()->getJobName() === 'magento2_category_export';
        } catch(\Exception $e) {
            $flag = false;
        }

        return $flag;
    }
}
