<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Connector\Writer;

use Akeneo\Component\Batch\Item\ItemWriterInterface;
use Akeneo\Component\Batch\Model\StepExecution;
use Akeneo\Component\Batch\Item\DataInvalidItem;
use MyIntegrations\Bundle\ConnectorBundle\Component\Normalizer\PropertiesNormalizer;
use MyIntegrations\Bundle\ConnectorBundle\Connector\Writer\BaseWriter;
use MyIntegrations\Bundle\ConnectorBundle\Entity\DataMapping;
use MyIntegrations\Bundle\ConnectorBundle\Services\ApiConnector;
use Symfony\Component\HttpFoundation\Response;
use MyIntegrations\Bundle\ConnectorBundle\Traits\DataMappingTrait;

class AttributeWriter extends BaseWriter implements ItemWriterInterface
{
    use DataMappingTrait;

    const AKENEO_ENTITY_NAME = 'attribute';
    const ERROR_ALREADY_EXIST = '%1 already exists.';

    protected $reservedKeys;

    public function __construct(\Doctrine\ORM\EntityManager $em, ApiConnector $connectorService)
    {
        $this->em = $em;
        $this->connectorService = $connectorService;
    }

    /**
     * write attributes to magento2 Api
     */
    public function write(array $items)
    {
        $parameters = $this->getParameters();
        $availableLocales = $this->getFilterLocales($this->stepExecution);
        $storeMapping = array_filter($this->getStoreMapping() );
        $locales = array_intersect($availableLocales, array_keys($storeMapping));

        $storeViews = json_decode($this->getCredentials()['storeViews'], true);
        $storeMappingIds = [];
        foreach($storeViews as $storeView) {
            $storeMappingIds[$storeView['code']] = $storeView['id'];
        }

        if(!$this->reservedKeys) {
            $attributeMappings = $this->connectorService->getAttributeMappings();
            $this->reservedKeys = array_merge(
                    array_unique(array_filter(array_values($attributeMappings))),
                    array_keys($attributeMappings),
                    $this->reservedAttributes
            );
        }

        while(count($items)) {
            $item = array_shift($items);
            $errorMsg = false;

            $locale = in_array($this->defaultLocale, array_keys($item['labels'])) ? $this->defaultLocale : array_keys($item['labels'])[0];
            $name = !empty($item['labels'][$locale]) ? $item['labels'][$locale] : null;
            $this->storeViewCode = $storeMapping[$locale];

            if(in_array($item['code'], $this->reservedKeys)) {
                $this->stepExecution->incrementSummaryInfo('skipped');
                continue;
            }

            /* validations */
            if(!$name) {
                /* show error if translation of category for locale is not present */
                $errorMsg = 'Untranslated attribute with code: ' . $item['code']  .  ', for locale: ' . $locale ;
                $this->stepExecution->addWarning($errorMsg , ['error' => true ], new DataInvalidItem([]));
                $this->stepExecution->incrementSummaryInfo('skipped');
                continue;
            }

            if(!array_key_exists($item['type'], $this->attributeTypes)) {
                // $msg = $item['type'] . 'attributes are not supported yet';
                // $this->stepExecution->addWarning($msg, [], new DataInvalidItem([ 'code' => $item['code'] ]) );
                $this->stepExecution->incrementSummaryInfo('skipped');
                continue;
            }

            $item['name'] = $name;
            $item['type'] = $this->attributeTypes[$item['type']];
            /* multi locale */
            $item['frontend_labels'] = [];
            foreach($locales as $locale) {
                /* get storeViewId by code and then use in data */
                if(!empty($storeMappingIds[$storeMapping[$locale]]) && !empty($item['labels'][$locale])) {
                    $item['frontend_labels'][] = [
                        'store_id' => $storeMappingIds[$storeMapping[$locale]],
                        'label'    => $item['labels'][$locale],
                    ];
                }
            }
            $mapping = $this->getMappingByCode($item['code'],  self::AKENEO_ENTITY_NAME, false);
            $data = $this->createArrayFromDataAndMatcher($item, $this->matcher , SELF::AKENEO_ENTITY_NAME);
            $data[self::AKENEO_ENTITY_NAME] = array_merge(
                    $data[self::AKENEO_ENTITY_NAME],
                    $this->fillers,
                    [
                        'is_html_allowed_on_front' => (boolean)$item['wysiwyg_enabled'],
                        'scope'                    => empty($item['localizable']) ? 'global' : 'store'
                    ]
                );

            if($mapping) {
                /* update resource */
                if($mapping->getExternalId()) {

                    $data[self::AKENEO_ENTITY_NAME]['attribute_id'] = $mapping->getExternalId();
                    $data[self::AKENEO_ENTITY_NAME]['entity_type_id'] = $mapping->getRelatedId();

                    $attribute = $this->addAttribute($data, $item['code']);

                    if(!empty($attribute['error']) && $attribute['code'] == Response::HTTP_NOT_FOUND) {
                        $mapping = $this->deleteMapping($mapping);
                    }
                }
            }

            if(!$mapping) {
                /* add resource */
                $attribute = $this->addAttribute($data);

                if(!empty($attribute['error'])) {
                    /* already exist */
                    if(!empty($attribute['error']['message'] ) /**&& self::ERROR_ALREADY_EXIST == $attribute['error']['message']**/ ) {
                        $attribute = $this->getAttributeByCode($item['code']);

                        /* re request to update */
                        if(!empty($attribute['attribute_id'])) {
                            $data[self::AKENEO_ENTITY_NAME]['attribute_id'] = $attribute['attribute_id'];
                            $data[self::AKENEO_ENTITY_NAME]['entity_type_id'] = $attribute['entity_type_id'];
                            $data[self::AKENEO_ENTITY_NAME]['backend_type'] = $attribute['backend_type'];

                            $this->addAttribute($data, $item['code']);
                            $this->addMappingByCode($item['code'], $attribute['attribute_id'], $attribute['entity_type_id']);
                        }
                    }
                } else if(!empty($attribute['attribute_id'])) {
                    $this->addMappingByCode(
                        $item['code'],
                        $attribute['attribute_id'],
                        !empty($attribute['entity_type_id']) ? $attribute['entity_type_id'] : null
                        );
                }
            }

            /* increment write count */
            if(!$errorMsg) {
                $this->stepExecution->incrementSummaryInfo('write');
            }
        }
    }

    protected function getAttributeByCode($code)
    {
        $method = 'GET';
        $url = $this->getApiUrlByEndpoint('updateAttributes', $this->storeViewCode);
        $url = str_replace('{attributeCode}', $code, $url);
        try {
            $this->oauthClient->fetch($url, null, $method, $this->jsonHeaders );
            $results = json_decode($this->oauthClient->getLastResponse(), true);
            return $results;
        } catch(\Exception $e) {
            $error = ['error' => json_decode($this->oauthClient->getLastResponse(), true) ];
            return $error;
        }
    }

    protected function addAttribute(array $resource, $code = null)
    {
        if(!empty($resource[self::AKENEO_ENTITY_NAME]['attribute_code'])) {
            $resource[self::AKENEO_ENTITY_NAME]['attribute_code'] = strtolower($resource[self::AKENEO_ENTITY_NAME]['attribute_code']);
        }
        if($code) {
            $code = strtolower($code);
            $method = 'PUT';
            $url = $this->getApiUrlByEndpoint('updateAttributes', $this->storeViewCode);
            $url = str_replace('{attributeCode}', $code, $url);
        } else {
            $method = 'POST';
            $url = $this->getApiUrlByEndpoint('attributes', $this->storeViewCode);
        }

        try {
            $this->oauthClient->fetch($url, json_encode($resource), $method, $this->jsonHeaders );
            $results = json_decode($this->oauthClient->getLastResponse(), true);
            $this->removeEmptyOptions($code, $results['options']);

            return $results;
        } catch(\Exception $e) {
            $info = $this->oauthClient->getLastResponseInfo();
            $error = [
                    'error' => json_decode($this->oauthClient->getLastResponse(), true),
                    'code' => isset($info['http_code']) ? $info['http_code'] : 0
                ];

            return $error;
        }
    }

    protected function removeEmptyOptions($code, $options)
    {
        if(!empty($options)) {
            foreach($options as $option) {
                if(trim($option['value']) && !trim($option['label'])) {
                    $this->removeOption($code, $option['value']);
                }
            }
        }
    }

    protected function removeOption($attributeCode, $optionId)
    {
        $url = $this->getApiUrlByEndpoint('deleteAttributeOption');
        $url = str_replace('{attributeCode}', $attributeCode, $url);
        $url = str_replace('{option}', $optionId, $url);
        $method = 'DELETE';

        try {
            $this->oauthClient->fetch($url, null, $method, $this->jsonHeaders );
            $results = $this->oauthClient->getLastResponse();
            return $results;
        } catch(\Exception $e) {
            $error = ['error' => json_decode($this->oauthClient->getLastResponse(), true) ];
            return $error;
        }
    }

    protected $matcher = [
        // akeneo_value              magento_value
        'wysiwyg_enabled'        => 'is_wysiwyg_enabled',
        'useable_as_grid_filter' => 'is_filterable',
        'useable_as_grid_filter' => 'is_filterable_in_search',
        'unique'                 => 'is_unique',
        'code'                   => 'attribute_code',
        'type'                   => 'frontend_input',

        'name'                   => 'default_frontend_label',
        'type'                   => 'frontend_input',
        'frontend_labels'        => 'frontend_labels',
        // 'sort_order'             => 'sort_order' //label, value, sort_order
        // 'validation_rule'        => 'validation_rules'
    ];

    protected $fillers = [
        'entity_type_id'           => 0,
        'is_searchable'            => 0,
        'is_comparable'            => 0,
        'is_used_for_promo_rules'  => 0,
        'used_in_product_listing'  => 1,
        'is_visible'               => true,
        'is_visible_on_front'      => true,
        // 'is_html_allowed_on_front' => false,
        // 'scope'                    => "store",
    ];


    protected $attributeTypes = [
        // 'pim_catalog_identifier' => 'text',

        'pim_catalog_text'          => 'text',
        'pim_catalog_number'        => 'text',
        'pim_catalog_textarea'      =>  'textarea',
        'pim_catalog_date'          => 'date',
        'pim_catalog_boolean'       => 'boolean',
        'pim_catalog_multiselect'   => 'multiselect',
        'pim_catalog_simpleselect'  => 'select',
        // 'pim_catalog_image'         => 'media_image',
        'pim_catalog_price_collection' => 'price',

        'pim_catalog_metric'        => 'text',
        // 'pim_catalog_file'          => 'media_image'
        // 'weee', //tax
        // 'swatch_visual', // visual swatch
        // 'swatch_text', // text swatch
    ];
}
