<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Connector\Writer;

use Akeneo\Component\Batch\Item\ItemWriterInterface;
use MyIntegrations\Bundle\ConnectorBundle\Component\Normalizer\PropertiesNormalizer;
use Akeneo\Component\Batch\Step\StepExecutionAwareInterface;
use Akeneo\Component\Batch\Model\StepExecution;
use Akeneo\Component\Batch\Item\DataInvalidItem;
use MyIntegrations\Bundle\ConnectorBundle\Connector\Writer\BaseWriter;
use MyIntegrations\Bundle\ConnectorBundle\Services\ApiConnector;
use MyIntegrations\Bundle\ConnectorBundle\Entity\DataMapping;
use MyIntegrations\Bundle\ConnectorBundle\Component\OAuthClient;
use MyIntegrations\Bundle\ConnectorBundle\Traits\DataMappingTrait;

class ProductWriter extends BaseWriter implements ItemWriterInterface
{
    use DataMappingTrait;

    const AKENEO_ENTITY_NAME = 'product';

    private $attributeRepo;
    private $familyVariantRepo;

    protected $locale;
    protected $baseCurrency;
    protected $weightUnit;

    public function __construct(\Doctrine\ORM\EntityManager $em, ApiConnector $connectorService, $attributeRepo, $familyVariantRepo)
    {
        $this->em = $em;
        $this->connectorService = $connectorService;
        $this->attributeRepo = $attributeRepo;
        $this->familyVariantRepo = $familyVariantRepo;
    }

    /**
     * write products to magento2 Api
     */
    public function write(array $items)
    {
        if(!$this->oauthClient) {
            $this->setStepExecution->addWarning('invalid oauth client', [], new DataInvalidItem());
            return;
        }
        $parameters = $this->getParameters();
        $availableLocales = $this->getFilterLocales($this->stepExecution);

        $storeMapping = array_filter($this->getStoreMapping() );
        $locales = array_intersect($availableLocales, array_keys($storeMapping));

        $storeSettings = $this->connectorService->getOtherSettings();

        foreach($items as $key => $mainItem) {
            foreach($locales as $iteration => $locale) {
                $this->locale = $locale;
                $this->storeViewCode = $storeMapping[$locale];
                $this->baseCurrency = !empty($storeSettings[$this->storeViewCode]['base_currency_code']) ? $storeSettings[$this->storeViewCode]['base_currency_code'] : null;
                $this->weightUnit = !empty($storeSettings[$this->storeViewCode]['weight_unit']) ? $storeSettings[$this->storeViewCode]['weight_unit'] : null;

                $item = $this->formatData($mainItem);
                if($iteration) {
                    $item['media_gallery_entries'] = [];
                }

                switch($item[PropertiesNormalizer::FIELD_MAGENTO_PRODUCT_TYPE]) {
                    case 'simple':
                        $product = [self::AKENEO_ENTITY_NAME => $item];
                        $product = $this->addProduct($product);
                        break;
                    case 'variant':
                        $item[PropertiesNormalizer::FIELD_MAGENTO_PRODUCT_TYPE] = PropertiesNormalizer::SIMPLE_TYPE;
                        $parent = $item['parent'];
                        unset($item['parent']);

                        /* add product model */
                        $mapping = $this->getMappingByCode($parent['sku'], self::AKENEO_ENTITY_NAME);
                        if($mapping && $this->stepExecution->getJobExecution()->getId() !== $mapping->getJobInstanceId() ) {
                            $this->em->remove($mapping);
                            $this->em->flush();
                            $mapping = null;
                        }
                        if(!$mapping) {
                            $product = $this->addProduct([self::AKENEO_ENTITY_NAME => $parent]); /* parent product */
                            if(!empty($product['id'])) {
                                $this->addMappingByCode($parent['sku'], $product['id']);
                                if(!$iteration) {
                                    $this->quickExportIncrement();
                                }
                            }
                        }


                        $itemResult = $this->addProduct([self::AKENEO_ENTITY_NAME => $item]);   /* child */
                        if($iteration == (-1+count($locales)) ) {
                            $childSku  = $item['sku'];
                            $parentSku = $parent['sku'];
                            if($itemResult && empty($itemResult['error'])) {
                                $this->linkConfigurableChildToParent($childSku, $parentSku);
                            }
                        }
                        break;
                    case 'virtual':
                        $productData = [self::AKENEO_ENTITY_NAME => $item];
                        $product = $this->addProduct($productData);
                        break;
                    case 'downloadable':
                    case 'bundle':
                    case 'grouped':
                        break;
                }
            }

            $this->stepExecution->incrementSummaryInfo('write');
        }
        return null;
    }

    protected function formatData($item)
    {
        $formatted = [
            'custom_attributes' => []
        ];

        foreach($item[PropertiesNormalizer::FIELD_META_DATA]['unprocessed'] as $index) {
            if(isset($item[$index])) {
                $formatted[$index] = $this->formatValueForMagento($item[$index]);
            }
        }

        foreach(['sku', 'media_gallery_entries', 'extension_attributes', PropertiesNormalizer::FIELD_VISIBILITY, PropertiesNormalizer::FIELD_MAGENTO_PRODUCT_TYPE] as $rawAttribute) {
            if(isset($item[$rawAttribute])) {
                $formatted[$rawAttribute] = $item[$rawAttribute];
            }
        }

        /* categories */
        $categories = !empty($item[PropertiesNormalizer::FIELD_META_DATA]['categories']) ? $item[PropertiesNormalizer::FIELD_META_DATA]['categories'] : [];
        $catIds = $this->getcategoryIdsFromCategories($categories);
        if(!empty($catIds)) {
            $item['custom_attributes'][] = [
                "attribute_code" => "category_ids",
                "value"          => $catIds
            ];
        }

        // $item = $this->selectValuesBasedOnLocaleAndCurrency($item);
        if(!empty($item[PropertiesNormalizer::FIELD_META_DATA]['unprocessed'])) {
            foreach($item[PropertiesNormalizer::FIELD_META_DATA]['unprocessed'] as $index) {
                if(isset($item[$index])) {
                    $formatted[$index] = $this->formatValueForMagento($item[$index]);
                }
            }
        }

        if(!empty($item['custom_attributes'])) {
            foreach($item['custom_attributes'] as $index => $value) {
                $value['value'] = $this->formatValueForMagento($value['value']);;
                $formatted['custom_attributes'][] = $value;
            }
        }

        if(!empty($formatted['custom_attributes'])) {
            $formatted['custom_attributes'] = $this->modifyOptionValues($formatted['custom_attributes']);
        }

        $family = $item[PropertiesNormalizer::FIELD_META_DATA]['family'];
        if($family) {
            $familyMapping = $this->getMappingByCode($family, 'family');
            if($familyMapping) {
                $formatted['attribute_set_id'] = $familyMapping->getExternalId();
            }
        }

        if(!empty($item['parent'])) {
            $formatted['parent'] = $this->formatData($item['parent']);
        }

        if(isset($item['axes']) && !empty($formatted['parent'])) {
            $axes = $item['axes'];
            if(!empty($axes)) {
                if(!isset($formatted['parent']['extension_attributes'])) {
                    $formatted['parent']['extension_attributes'] = [];
                }
                if(!isset($formatted['parent']['extension_attributes']['stock_item'])) {
                    $formatted['parent']['extension_attributes']['stock_item'] = [
                        'is_in_stock' => true
                    ];
                }

                $formatted['parent']['extension_attributes']['configurable_product_options'] = [];
                foreach($axes as $attributeCode) {
                    $mapping = $this->getMappingByCode($attributeCode, 'attribute');
                    if($mapping) {
                        $customAttr = [
                            'attribute_id' =>  $mapping->getExternalId(),
                            'label' => $attributeCode,
                            'position' =>  0,
                            'is_use_default' => true,
                            'values' => [
                            ]
                        ] ;
                        $optionMappings = $this->getOptionsByAttributeCode($attributeCode);
                        if($optionMappings) {
                            foreach($optionMappings as $optionMapping) {
                                $customAttr['values'][] = [
                                    'value_index' => $optionMapping->getExternalId(),
                                ];
                            }
                        }

                        $formatted['parent']['extension_attributes']['configurable_product_options'][] = $customAttr;
                    }
                }
            }
        }

        return $formatted;
    }


    protected function modifyOptionValues($data)
    {
        $attributeMappings = $this->connectorService->getAttributeMappings();

        foreach($data as $index => $attr) {
            $realAttrCode = !empty($attributeMappings[$attr['attribute_code']]) ? $attributeMappings[$attr['attribute_code']] : $attr['attribute_code'];
            $attribute = $this->attributeRepo->findOneByIdentifier($realAttrCode);
            if(empty($attribute)) {
                continue;
            }

            if('pim_catalog_date' == $attribute->getType()) {
                $data[$index]['value'] = $this->formatDate($attr['value']);
            } else if(in_array($attribute->getType(), $this->simpleAttributeTypes )) {
            } else if(in_array($attribute->getType(), $this->selectAttributeTypes )) {
                if(is_array($attr['value'])) {
                    $data[$index]['value'] = [];
                    foreach($attr['value'] as $singleValue) {
                        $attributeOption = $this->getMappingByCode($singleValue. '(' . $attr['attribute_code'] . ')', 'option');
                        if($attributeOption) {
                            $data[$index]['value'][] = (int)$attributeOption->getExternalId();
                        }
                    }
                } elseif(gettype($attr['value']) == 'string') {
                    $attributeOption = $this->getMappingByCode($attr['value']. '(' . $attr['attribute_code'] . ')', 'option');
                    if($attributeOption) {
                        $data[$index]['value'] = $attributeOption->getExternalId();
                    } else {
                        unset($data[$index]);
                    }
                }
            }

            if(!empty($data[$index])) {
                $data[$index]['attribute_code'] = strtolower($data[$index]['attribute_code']);
            }
        }

        return $data;
    }

    protected function getcategoryIdsFromCategories($categories)
    {
        $catIds = [];
        foreach($categories as $categoryCode) {
            $mapping = $this->getMappingByCode($categoryCode, 'category');
            if($mapping && $mapping->getExternalId()) {
                $catIds[] = $mapping->getExternalId() ;
            }
        }

        return $catIds;
    }

    protected function linkCategories($categories, $product)
    {
        if(empty($product['sku'])) {
            return;
        }

       foreach($categories as $categoryCode) {
           $mapping = $this->getMappingByCode($categoryCode, 'category');
           if($mapping && $mapping->getExternalId()) {
               $data = [
                    'productLink' => [
                        'sku' => $product['sku'],
                        'category_id' => $mapping->getExternalId(),
                        'position' => 0,
                    ]
               ];

                $url = $this->getApiUrlByEndpoint('addCategoryToProduct');
                $url = str_replace('{categoryId}', $mapping->getExternalId() , $url);

                try {
                    $this->oauthClient->fetch($url, json_encode($data), 'POST', $this->jsonHeaders );
                } catch(\Exception $e) {
                    $this->stepExecution->addWarning($this->oauthClient->getLastResponse() .json_encode($data)  , ['error' => true ], new DataInvalidItem([]));
                }
           }
       }
    }


    protected function linkConfigurableChildToParent($childSku, $parentSku)
    {
        if(!empty($childSku) && !empty($parentSku) ) {
            $url = $this->getApiUrlByEndpoint('addChild');
            $url = str_replace('{sku}', $parentSku, $url);

            $postData = [
                'childSku' => $childSku
            ];

            try {
                $this->oauthClient->fetch($url, json_encode($postData), 'POST', $this->jsonHeaders );
                // $this->stepExecution->addSummaryInfo($childSku ,$this->oauthClient->getLastResponse());
            } catch(\Exception $e) {
                $this->stepExecution->addWarning(
                    'link sku:' . $this->oauthClient->getLastResponse(),
                    ['error' => true ],
                    new DataInvalidItem(['sku' => $childSku])
                );
            }
        }
    }

    private function selectValuesBasedOnLocaleAndCurrency($item)
    {
        if(!empty($item['parent'])) {
            $item['parent'] = $this->selectValuesBasedOnLocaleAndCurrency($item['parent']);
        }
        if(!empty($item[PropertiesNormalizer::FIELD_META_DATA]['unprocessed'])) {
            foreach($item[PropertiesNormalizer::FIELD_META_DATA]['unprocessed'] as $index) {
                if(isset($item[$index])) {
                    $item[$index] = $this->formatValueForMagento($item[$index]);
                }
            }
        }
        if(!empty($item['custom_attributes'])) {
            foreach($item['custom_attributes'] as $index => $value) {
                $item['custom_attributes'][$index]['value'] = $this->formatValueForMagento($value['value']);
            }
        }

        return $item;
    }

    private function formatValueForMagento($value)
    {
        if(is_array($value)) {
            foreach($value as $key => $aValue) {
                if(is_array($aValue) ) {
                    if(array_key_exists('locale', $aValue)) {
                        if(!$aValue['locale'] || $aValue['locale'] == $this->locale) {
                            $value = $aValue['data'];
                            break;
                        }
                        if($key == count($value)-1) {
                            $value = null;
                        }
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        if($value && is_array($value) ) {
            /* price */
            foreach($value as $key => $aValue) {
                if(is_array($aValue)) {
                    if(array_key_exists('currency', $aValue)) {
                        if(!$aValue['currency'] || $aValue['currency'] == $this->baseCurrency) {
                            $value = (float)$aValue['amount'];
                            break;
                        }
                        if($key == count($value)-1) {
                            $value = !empty($value[0]['amount']) ? $value[0]['amount'] : null ;
                            $value = (float)$value;
                        }
                    }

                } else {
                    break;
                }
            }
            /* metric */
            if(is_array($value) && array_key_exists('unit', $value)) {
                $value = !empty($value['amount']) ? $value['amount'] : null;
            }
        }
        if(null === $value) {
            $value = '';
        }

        return $value;
    }


    /* attribute sets */
    private $attributeSet;
    public function getAttributeSets()
    {
        if(!$this->attributeSet) {
            $url = $this->getApiUrlByEndpoint('attributeSets');
            $this->oauthClient->fetch($url, null, 'GET', $this->jsonHeaders);
            $this->attributeSet = json_decode($this->oauthClient->getLastResponse(), true);

        }

        return $this->attributeSet;
    }

    protected function addProduct($productData)
    {
        $productData = $this->checkProductAndModifyData($productData);

        if(!empty($productData) ) {
            $productAddUrl = $this->getApiUrlByEndpoint(self::AKENEO_ENTITY_NAME, $this->storeViewCode);

            var_dump($productData);

            try {
                $this->oauthClient->fetch($productAddUrl, is_array($productData) ? json_encode($productData) : $productData, 'POST', $this->jsonHeaders );
                /* log success */
                return json_decode($this->oauthClient->getLastResponse(), true);
            } catch(\Exception $e) {
                /* log error */
                $this->stepExecution->addWarning($this->oauthClient->getLastResponse() , ['error' => true ], new DataInvalidItem([
                    'sku' => !empty($productData[self::AKENEO_ENTITY_NAME]['sku']) ? $productData[self::AKENEO_ENTITY_NAME]['sku'] : ''
                ]));

                return ['error' => json_decode($this->oauthClient->getLastResponse(), true)];
            }
        }
    }

    protected function checkProductAndModifyData($productData)
    {
        $url = $this->getApiUrlByEndpoint('getProduct');
        $url = str_replace('{sku}', $productData[self::AKENEO_ENTITY_NAME]['sku'], $url);

        /* fetch product */
        try {
            $this->oauthClient->fetch($url, null, 'GET', $this->jsonHeaders);
            $response = json_decode($this->oauthClient->getLastResponse(), true);
        } catch(\Exception $e) {
            $response = [];
        }

        $existingImages = [];
        if(!empty($response['media_gallery_entries'])) {
            foreach($response['media_gallery_entries'] as $image) {
                if(isset($image['file'])) {
                    $existingImages[] = $image['file'];
                }
            }
        }

        if(!empty($existingImages)) {
            $mediaEntries = $productData[self::AKENEO_ENTITY_NAME]['media_gallery_entries'];
            foreach($mediaEntries as $key => $mediaEntry) {
                if(isset($mediaEntry['content']['name'])) {
                    $nameArray = explode('.', $mediaEntry['content']['name'] );
                    $name = reset($nameArray);
                    if(count(preg_grep('#' . $name . '#', $existingImages))) {
                        unset($mediaEntries[$key]);
                    }
                }
            }
            $productData[self::AKENEO_ENTITY_NAME]['media_gallery_entries'] = $mediaEntries;
        }

        return $productData;
    }

    protected function quickExportIncrement()
    {
        $params = $this->getParameters();
        $isQuickExport = !empty($params['filters']['0']['context']);

        if($isQuickExport) {
            $this->stepExecution->incrementSummaryInfo('write');
        }
    }

    private function getOptionsByAttributeCode($code)
    {
        $mappings = $this->mappingRepository->getOptionsByAttributeCode($code);

        return $mappings;
    }

    private function formatDate($date)
    {
        $dateObj = new \DateTime($date);

        return $dateObj->format('Y-m-d H:i:s');
    }

    protected $simpleAttributeTypes = ['pim_catalog_text', 'pim_catalog_number','pim_catalog_textarea','pim_catalog_date','pim_catalog_boolean'];

    protected $selectAttributeTypes = ['pim_catalog_multiselect', 'pim_catalog_simpleselect'];
}
