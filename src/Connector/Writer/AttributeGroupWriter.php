<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Connector\Writer;

use Akeneo\Component\Batch\Item\ItemWriterInterface;
use Akeneo\Component\Batch\Step\StepExecutionAwareInterface;
use Akeneo\Component\Batch\Model\StepExecution;
use Akeneo\Component\Batch\Item\DataInvalidItem;
use MyIntegrations\Bundle\ConnectorBundle\Component\Normalizer\PropertiesNormalizer;
use MyIntegrations\Bundle\ConnectorBundle\Connector\Writer\BaseWriter;
use MyIntegrations\Bundle\ConnectorBundle\Entity\DataMapping;
use MyIntegrations\Bundle\ConnectorBundle\Services\ApiConnector;
use MyIntegrations\Bundle\ConnectorBundle\Traits\DataMappingTrait;
use Symfony\Component\HttpFoundation\Response;

class AttributeGroupWriter extends BaseWriter implements ItemWriterInterface
{
    use DataMappingTrait;

    const AKENEO_ENTITY_NAME = 'option';
    const ERROR_ALREADY_EXIST = '%1 already exists.';
    const ERROR_DELETED = 'Cannot save attribute %1';

    protected $fetchedOptions = [];

    public function __construct(\Doctrine\ORM\EntityManager $em, ApiConnector $connectorService)
    {
        $this->em = $em;
        $this->connectorService = $connectorService;
    }

    /**
     * write attribute options to magento2 Api
     */
    public function write(array $items)
    {
//        $parameters = $this->getParameters();
//        $availableLocales = $this->getFilterLocales($this->stepExecution);
//        $storeMapping = array_filter($this->getStoreMapping() );
//        $locales = array_intersect($availableLocales, array_keys($storeMapping));
//        $storeViews = json_decode($this->getCredentials()['storeViews'], true);
//        $storeMappingIds = [];
//        foreach($storeViews as $storeView) {
//            $storeMappingIds[$storeView['code']] = $storeView['id'];
//        }
//
//        while(count($items)) {
//            $item = array_shift($items);
//            $errorMsg = false;
//            $locale = in_array($this->defaultLocale, array_keys($item['labels'])) ? $this->defaultLocale : array_keys($item['labels'])[0];
//            $name = $item['labels'][$locale];
//            $this->storeViewCode = $storeMapping[$locale];
//
//            /* multi locale */
//            $item['store_labels'] = [];
//
//            foreach($locales as $locale) {
//                $localeLabel = isset($item['labels'][$locale]) ? $item['labels'][$locale] : null;
//                if($localeLabel) {
//                    $item['store_labels'][] = [
//                        'store_id' =>  $storeMappingIds[$storeMapping[$locale]],
//                        'label'    => $localeLabel,
//                    ];
//                }
//            }
//
//            $mapping = $this->getMappingByCode($item['code'] . '(' . $item['attribute'] . ')');
//            $data = $this->createArrayFromDataAndMatcher($item, $this->matcher , SELF::AKENEO_ENTITY_NAME);
//
//            if(!$mapping) {
//                $existingId = $this->checkExistingId($item);
//                if($existingId) {
//                    $mapping = $this->addMappingByCode($item['code'] . '(' . $item['attribute'] . ')', $existingId);
//                }
//            }
//
//
//            if($mapping) {
//                /* update resource */
//                if($mapping->getExternalId()) {
//                    $data[self::AKENEO_ENTITY_NAME]['value'] = $mapping->getExternalId();
//
//                    $attribute = $this->addAttributeOption($data, $item['attribute']);
//                    if(!empty($attribute['error']) && $attribute['code'] == Response::HTTP_BAD_REQUEST) {
//                        $mapping = $this->deleteMapping($mapping);
//                    }
//                }
//            }
//
//            if(!$mapping) {
//                /* add resource */
//                $option = $this->addAttributeOption($data, $item['attribute']);
//                if(!isset($option['error'])) {
//                    $getOptions = $this->getAttributeOptions($item['attribute'], true);
//                    if(is_array($getOptions) && empty($getOptions['error'])) {
//                        $labelId = $this->searchLabelAndRemove($name, $getOptions, $item['attribute']);
//                        if($labelId) {
//                            $this->addMappingByCode($item['code'] . '(' . $item['attribute'] . ')', $labelId);
//                        }
//                    }
//                }
//            }
//
//            /* increment write count */
//            if(!$errorMsg) {
//                $this->stepExecution->incrementSummaryInfo('write');
//            }
//        }
    }

    protected $matcher = [
        // akeneo_key     =>           external_key
        'sort_order'             => 'sort_order',
        'code'                   => 'label',
        'store_labels'           => 'store_labels',
    ];
}
