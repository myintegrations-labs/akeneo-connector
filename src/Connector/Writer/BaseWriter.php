<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Connector\Writer;

use Akeneo\Component\Batch\Item\ItemWriterInterface;
use Akeneo\Component\Batch\Step\StepExecutionAwareInterface;
use Akeneo\Component\Batch\Model\StepExecution;
use Akeneo\Component\Batch\Item\DataInvalidItem;
use MyIntegrations\Bundle\ConnectorBundle\Connector\Writer\Model;
use MyIntegrations\Bundle\ConnectorBundle\Component\Normalizer\PropertiesNormalizer;
use MyIntegrations\Bundle\ConnectorBundle\Component\OAuthClient;
use MyIntegrations\Bundle\ConnectorBundle\Traits\StepExecutionTrait;

class BaseWriter implements StepExecutionAwareInterface
{
    use StepExecutionTrait;

    /** @var StepExecution */
    protected $stepExecution;

    /* @var params for connector */
    private $params;

    protected $oauthClient;

    protected $jobInstance;

    protected $em;

    protected $mappingRepository;

    protected $connectorService;

    protected $credentials;

    protected $storeViewCode;

    protected $defaultLocale = 'en_US';

    protected $storeMapping;

    private $storeConfigs;

    protected $reservedAttributes = [
                                    'sku', 'name','weight', 'status', 'description', 'short_description', 'price', 'visibility', 'weight',
                                    'tax_class_id', 'quantity_and_stock_status', 'category_ids', 'tier_price', 'price_view', 'gift_message_available'
                                ];

    /* @var default json headers for api */
    protected $jsonHeaders = ['Content-Type' => 'application/json', 'Accept' => 'application/json'];

    /**
     * {@inheritdoc}
     */
    public function setStepExecution(StepExecution $stepExecution)
    {
        $this->stepExecution = $stepExecution;
        $this->initializeApiRequirents();
    }

    public function getParameters()
    {
        if(!$this->stepExecution) {
            throw new \Exception('call setStepExecution first');
            return [];
        }
        if(!$this->params) {
            $this->params = $this->stepExecution->getJobParameters()->all();
        }

        return $this->params;
    }

    public function getStoreMapping()
    {
        $storeMapping = !empty($this->credentials['storeMapping']) ? $this->credentials['storeMapping'] : [];
        $this->storeMapping = array_filter($storeMapping);
        return $this->storeMapping;
    }


    public function getCredentials()
    {
        if(!$this->credentials) {
            $this->credentials = $this->connectorService->getCredentials();
        }

        return $this->credentials;
    }

    // public function getStoreConfigs()
    // {
    //     if($this->oauthClient && !$this->storeConfigs) {
    //         $url = $this->getApiUrlByEndpoint('storeConfigs');
    //         $method = 'GET';
    //         try {
    //             $this->oauthClient->fetch($url, null, $method, $this->jsonHeaders );
    //             $this->storeConfigs = json_decode($this->oauthClient->getLastResponse(), true);
    //         } catch(\Exception $e) {
    //         }
    //     }

    //     return $this->storeConfigs;
    // }

    protected function initializeApiRequirents()
    {
        $this->getCredentials();
        $this->createOauthClient();
        if($this->em) {
            $this->mappingRepository = $this->em->getRepository('Magento2Bundle:DataMapping');
        }
        // $this->getStoreConfigs();

        if(is_array($this->storeConfigs) && count($this->storeConfigs) && !empty(reset($this->storeConfigs)['locale'])) {
            $this->defaultLocale = reset($this->storeConfigs)['locale'];
        }
    }


    protected function getJobInstance()
    {
        if(!$this->stepExecution) {
            throw new \Exception('call setStepExecution first');
            return [];
        }
        if(!$this->jobInstance) {
            $this->jobInstance = $this->stepExecution->getJobExecution()->getJobInstance();
        }

        return $this->jobInstance;
    }


    protected function getHostName()
    {
        try {
            $result = rtrim($this->credentials['hostName'], '/');
        } catch(\Exception $e) {
            $result = null;
        }

        return $result;
    }

    protected function createOauthClient()
    {
        try {
            $params = $this->credentials;
            $oauthClient = new OAuthClient(
                !empty($params['authToken']) ? $params['authToken'] : null
                );
            $url = $params['hostName'] . '/rest/V1/store/storeViews';

            $this->oauthClient = $oauthClient;
        } catch(\Exception $e) {
        }

        return $this->oauthClient;
    }

    /**
    * filters data from array and add data into wrapper element
    *
    * @param array $item
    * @param array $matcher
    * @param string $wrapper
    *
    * @return array $data to post to magento2 api
    */
    protected function createArrayFromDataAndMatcher($item, $matcher, $wrapper = null)
    {
        $data = [];
        foreach($matcher as $akeneoKey => $externalKey) {
            if(array_key_exists($akeneoKey, $item)) {
                $data[$externalKey] = $item[$akeneoKey];
            }
        }

        return $wrapper ? [$wrapper => $data] : $data;
    }


    protected function getApiUrlByEndpoint($endpointName, $store = '')
    {
        $store = $store ? '/' . $store . '/' : '/';

        if(array_key_exists($endpointName, $this->apiEndpoints) ) {
            $url = $this->getHostName() . $this->apiEndpoints[$endpointName];
            return str_replace('/{_store}/', $store, $url);
        }

        return null;
    }

    public function fetch($url, $payload)
    {
        try {
            $this->oauthClient->fetch($url, [], 'GET', ['Content-Type' => 'application/json', 'Accept' => 'application/json'] );

            $results = $this->oauthClient->getLastResponse();
        } catch(\Exception $e) {
            $results = null;
        }
    }

    private $apiEndpoints = [
        'storeViews'          => '/rest/V1/store/storeViews',
        'storeConfigs'        => '/rest/V1/store/storeConfigs',
        'product'             => '/rest/{_store}/V1/products/?searchCriteria=',
        'currency'            => '/rest/{_store}/V1/directory/currency',
        'getAttributeSets'    => '/rest/{_store}/V1/products/attribute-sets/sets/list?searchCriteria[pageSize]=50',
        'addAttributeSet'     => '/rest/{_store}/V1/products/attribute-sets',
        'updateAttributeSet'  => '/rest/{_store}/V1/products/attribute-sets/{attributeSetId}',
        'addToAttributeSet'   => '/rest/{_store}/V1/products/attribute-sets/attributes',
        'getAttributeSet'     => '/rest/{_store}/V1/products/attribute-sets/{attributeSetId}/attributes',
        'configurableOptions' => '/rest/{_store}/V1/configurable-products/{sku}/options/all',
        'addChild'            => '/rest/{_store}/V1/configurable-products/{sku}/child',
        'categories'          => '/rest/{_store}/V1/categories?searchCriteria=',
        'updateCategory'      => '/rest/{_store}/V1/categories/{category}',
        'attributes'          => '/rest/{_store}/V1/products/attributes?searchCriteria=',
        'updateAttributes'    => '/rest/{_store}/V1/products/attributes/{attributeCode}',
        'attributeOption'     => '/rest/{_store}/V1/products/attributes/{attributeCode}/options',
        'deleteAttributeOption'=> '/rest/{_store}/V1/products/attributes/{attributeCode}/options/{option}',
        'addAttributeGroup'   => '/rest/{_store}/V1/products/attribute-sets/groups',
        'getAttributeGroup'   => '/rest/{_store}/V1/products/attribute-sets/groups/list?searchCriteria=',
        'addCategoryToProduct' => '/rest/{_store}/V1/categories/{categoryId}/products',
        'getProduct'          => '/rest/{_store}/V1/products/{sku}?searchCriteria=',
    ];

}
