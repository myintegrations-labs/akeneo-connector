<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Connector\Writer;

use Akeneo\Component\Batch\Item\ItemWriterInterface;
use Akeneo\Component\Batch\Step\StepExecutionAwareInterface;
use Akeneo\Component\Batch\Model\StepExecution;
use Akeneo\Component\Batch\Item\DataInvalidItem;
use MyIntegrations\Bundle\ConnectorBundle\Component\Normalizer\PropertiesNormalizer;
use MyIntegrations\Bundle\ConnectorBundle\Connector\Writer\BaseWriter;
use MyIntegrations\Bundle\ConnectorBundle\Entity\DataMapping;
use MyIntegrations\Bundle\ConnectorBundle\Services\ApiConnector;
use MyIntegrations\Bundle\ConnectorBundle\Traits\DataMappingTrait;
use Symfony\Component\HttpFoundation\Response;

class AttributeOptionWriter extends BaseWriter implements ItemWriterInterface
{
    use DataMappingTrait;

    const AKENEO_ENTITY_NAME = 'option';
    const ERROR_ALREADY_EXIST = '%1 already exists.';
    const ERROR_DELETED = 'Cannot save attribute %1';

    protected $fetchedOptions = [];

    public function __construct(\Doctrine\ORM\EntityManager $em, ApiConnector $connectorService)
    {
        $this->em = $em;
        $this->connectorService = $connectorService;
    }

    /**
     * write attribute options to magento2 Api
     */
    public function write(array $items)
    {
        $parameters = $this->getParameters();
        $availableLocales = $this->getFilterLocales($this->stepExecution);
        $storeMapping = array_filter($this->getStoreMapping() );
        $locales = array_intersect($availableLocales, array_keys($storeMapping));
        $storeViews = json_decode($this->getCredentials()['storeViews'], true);
        $storeMappingIds = [];
        foreach($storeViews as $storeView) {
            $storeMappingIds[$storeView['code']] = $storeView['id'];
        }

        while(count($items)) {
            $item = array_shift($items);
            $errorMsg = false;
            $locale = in_array($this->defaultLocale, array_keys($item['labels'])) ? $this->defaultLocale : array_keys($item['labels'])[0];
            $name = $item['labels'][$locale];
            $this->storeViewCode = $storeMapping[$locale];

            /* multi locale */
            $item['store_labels'] = [];

            foreach($locales as $locale) {
                $localeLabel = isset($item['labels'][$locale]) ? $item['labels'][$locale] : null;
                if($localeLabel) {
                    $item['store_labels'][] = [
                        'store_id' =>  $storeMappingIds[$storeMapping[$locale]],
                        'label'    => $localeLabel,
                    ];
                }
            }

            $mapping = $this->getMappingByCode($item['code'] . '(' . $item['attribute'] . ')');
            $data = $this->createArrayFromDataAndMatcher($item, $this->matcher , SELF::AKENEO_ENTITY_NAME);

            if(!$mapping) {
                $existingId = $this->checkExistingId($item);
                if($existingId) {
                    $mapping = $this->addMappingByCode($item['code'] . '(' . $item['attribute'] . ')', $existingId);
                }
            }


            if($mapping) {
                /* update resource */
                if($mapping->getExternalId()) {
                    $data[self::AKENEO_ENTITY_NAME]['value'] = $mapping->getExternalId();

                    $attribute = $this->addAttributeOption($data, $item['attribute']);
                    if(!empty($attribute['error']) && $attribute['code'] == Response::HTTP_BAD_REQUEST) {
                        $mapping = $this->deleteMapping($mapping);
                    }
                }
            }

            if(!$mapping) {
                /* add resource */
                $option = $this->addAttributeOption($data, $item['attribute']);
                if(!isset($option['error'])) {
                    $getOptions = $this->getAttributeOptions($item['attribute'], true);
                    if(is_array($getOptions) && empty($getOptions['error'])) {
                        $labelId = $this->searchLabelAndRemove($name, $getOptions, $item['attribute']);
                        if($labelId) {
                            $this->addMappingByCode($item['code'] . '(' . $item['attribute'] . ')', $labelId);
                        }
                    }
                }
            }

            /* increment write count */
            if(!$errorMsg) {
                $this->stepExecution->incrementSummaryInfo('write');
            }
        }
    }

    protected function checkExistingId($item)
    {
        $getOptions = $this->getAttributeOptions($item['attribute']);
        $existingId = null;
        if($getOptions) {
            foreach($getOptions as $getOption) {
                if(!empty(trim($getOption['label'])) && strtolower($getOption['label']) === $item['code']) {
                    $existingId = $getOption['value'];
                    break;
                }
            }
        }

        return $existingId;
    }

    protected function searchLabelAndRemove($label, $allOptions, $attributeCode)
    {
        $id = null;
        foreach($allOptions as $option) {
            if(!empty(trim($option['label'])) && $option['label'] == $label) {
                if(!$id) {
                    $id = $option['value'];
                } else if(empty(trim($option['label']))) {
                    $this->removeOption($attributeCode, $option['value']);
                }
            }
        }
        return $id;
    }

    protected function removeOption($attributeCode, $optionId)
    {
        $url = $this->getApiUrlByEndpoint('deleteAttributeOption');
        $url = str_replace('{attributeCode}', $attributeCode, $url);
        $url = str_replace('{option}', $optionId, $url);
        $method = 'DELETE';

        try {
            $this->oauthClient->fetch($url, null, $method, $this->jsonHeaders );
            $results = $this->oauthClient->getLastResponse();
            return $results;
        } catch(\Exception $e) {
            $error = ['error' => json_decode($this->oauthClient->getLastResponse(), true) ];
            return $error;
        }
    }

    protected function getAttributeOptions($attributeCode, $noCache = false)
    {
        if(!$noCache && !empty($fetchedOptions[$attributeCode])) {
            return $fetchedOptions[$attributeCode];
        }

        $url = $this->getApiUrlByEndpoint('attributeOption');
        $url = str_replace('{attributeCode}', $attributeCode, $url);
        $method = 'GET';

        try {
            $this->oauthClient->fetch($url, null, $method, $this->jsonHeaders );
            $results = json_decode($this->oauthClient->getLastResponse(), true);
            $fetchedOptions[$attributeCode] = $results;

            return $results;
        } catch(\Exception $e) {
            $error = ['error' => json_decode($this->oauthClient->getLastResponse(), true) ];

            return $error;
        }
    }

    protected function addAttributeOption(array $resource, $attributeCode)
    {
        $url = $this->getApiUrlByEndpoint('attributeOption');
        $url = str_replace('{attributeCode}', $attributeCode, $url);
        $method = 'POST';

        try {
            $this->oauthClient->fetch($url, json_encode($resource), $method, $this->jsonHeaders );
            $results = json_decode($this->oauthClient->getLastResponse(), true);
            return $results;
        } catch(\Exception $e) {
            $info = $this->oauthClient->getLastResponseInfo();
            $error = [
                'error' => json_decode($this->oauthClient->getLastResponse(), true),
                'code'  => isset($info['http_code']) ? $info['http_code'] : 0
                ];

            return $error;
        }
    }

    protected $matcher = [
        // akeneo_key     =>           external_key
        'sort_order'             => 'sort_order',
        'code'                   => 'label',
        'store_labels'           => 'store_labels',
    ];
}
