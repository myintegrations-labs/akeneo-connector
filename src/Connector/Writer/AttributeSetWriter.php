<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Connector\Writer;

use Akeneo\Component\Batch\Item\ItemWriterInterface;
use Akeneo\Component\Batch\Step\StepExecutionAwareInterface;
use Akeneo\Component\Batch\Model\StepExecution;
use Akeneo\Component\Batch\Item\DataInvalidItem;
use MyIntegrations\Bundle\ConnectorBundle\Component\Normalizer\PropertiesNormalizer;
use MyIntegrations\Bundle\ConnectorBundle\Connector\Writer\BaseWriter;
use MyIntegrations\Bundle\ConnectorBundle\Entity\DataMapping;
use MyIntegrations\Bundle\ConnectorBundle\Services\ApiConnector;
use Symfony\Component\HttpFoundation\Response;
use MyIntegrations\Bundle\ConnectorBundle\Traits\DataMappingTrait;

class AttributeSetWriter extends BaseWriter implements ItemWriterInterface
{
    use DataMappingTrait;

    const AKENEO_ENTITY_NAME = 'family';
    const ERROR_EXISTS = 'An attribute set named';
    const ATTRIBUTE_SET_ADD_ERROR = 'Cannot save attributeGroup';
    const GROUP_CODE = 'attributes_akeneo';
    const GROUP_NAME = 'Attributes';

    protected $defaultAttributeSetId;
    protected $reservedKeys = null;
    protected $fallbackAttributesetValue = 4;

    public function __construct(\Doctrine\ORM\EntityManager $em, ApiConnector $connectorService)
    {
        $this->em = $em;
        $this->connectorService = $connectorService;
    }

    /**
     * write attributeSets to magento2 Api
     */
    public function write(array $items)
    {
        $parameters = $this->getParameters();
        $availableLocales = $this->getFilterLocales($this->stepExecution);
        $storeMapping = array_filter($this->getStoreMapping() );
        $locales = array_intersect($availableLocales, array_keys($storeMapping));

        while(count($items)) {
            $item = array_shift($items);
            $errorMsg = false;
            $locale = in_array($this->defaultLocale, array_keys($item['labels'])) ? $this->defaultLocale : array_keys($item['labels'])[0];
            $this->storeViewCode = $storeMapping[$locale];

            $itemForMagento = [
                'attributeSet' => [
                    "attribute_set_name" => $item['code'],
                    "sort_order"         => 0,
                ]
            ];
            $mapping = $this->getMappingByCode($item['code']);

            if($mapping) {
                if(!$this->checkAttributeSetFromApi($mapping->getExternalId())) {
                    $mapping = $this->deleteMapping($mapping);
                }
            }

            if(!$mapping) {
                /* add attributeSet */
                $itemForMagento['skeletonId'] = ($this->getDefaultAttributeSetId());
                $resource = $this->postAttributeSetToApi($itemForMagento);

                if(!empty($resource['attribute_set_id'])) {
                    $id = $resource['attribute_set_id'];
                    $this->addMappingByCode($item['code'], $id, $resource['entity_type_id']);
                    $this->linkAttributesToAttributeSet($item, $id);
                } else if(isset($resource['error']['http_code']) && Response::HTTP_BAD_REQUEST == $resource['error']['http_code'] ) {
                    /* existing family */
                    $attrSets = $this->getAndAddAttributeSets();
                    $mapping = $this->getMappingByCode($item['code']);
                }
            }

            if($mapping) {
                $id = $mapping->getExternalId();
                $skeletonId = $mapping->getRelatedId();
                $itemForMagento['attributeSet']['entity_type_id'] = $skeletonId;
                $itemForMagento['attributeSet']['attribute_set_id'] = $id;

                $attributeSet = $this->postAttributeSetToApi($itemForMagento, 'update', $id);

                if((!empty($attributeSet['error']['http_code'])) && $attributeSet['error']['http_code'] == Response::HTTP_NOT_FOUND) {
                    // $this->handleDeletedEntity($item, $itemForMagento, $mapping);
                } else {
                    $this->linkAttributesToAttributeSet($item, $id );
                }
            }

            /* increment write count */
            if(!$errorMsg) {
                $this->stepExecution->incrementSummaryInfo('write');
            }
        }
    }

    protected function linkAttributesToAttributeSet($item, $attributeSetId)
    {
        if(!$this->reservedKeys) {
            $attributeMappings = $this->connectorService->getAttributeMappings();
            $this->reservedKeys = array_merge(
                    array_unique(array_filter(array_values($attributeMappings))),
                    array_keys($attributeMappings),
                    $this->reservedAttributes
            );
        }

        // $alreadyLinkedAttributes = $this->getAttributesInAttributeSet($attributeSetId);
        $alreadyLinkedAttributes = [];

        if(!empty($item['attributes'])) {
            foreach($item['attributes'] as $key => $attributeCode ) {
                if(in_array($attributeCode, $alreadyLinkedAttributes) || in_array($attributeCode, $this->reservedKeys) ) {
                    continue;
                }
                $attrMapping = $this->getMappingByCode($attributeCode, 'attribute');
                if(!$attrMapping) {
                    continue;
                }

                $groupId = null;
                $group = $this->getGroupMappingByCode(self::GROUP_CODE, $attributeSetId);

                if(!$group) {
                    $gData = [
                        'attribute_set_id'       => $attributeSetId,
                        'attribute_group_name'  => self::GROUP_NAME,
                        'extension_attributes' => [
                            'attribute_group_code' => self::GROUP_CODE
                        ]
                    ];
                    $group = $this->addGroup(['group' => $gData]);
                    if(!empty($group['error']['message']) /**&& self::ATTRIBUTE_SET_ADD_ERROR == $group['error']['message']**/) {
                        $group = $this->getAttributeGroupByName(self::GROUP_NAME, $attributeSetId);
                    }
                    if(!empty($group['attribute_group_id'])) {
                        $this->addMappingByCode(self::GROUP_CODE, $group['attribute_group_id'], $group['attribute_set_id'], 'group' );
                        $groupId = $group['attribute_group_id'];
                    }
                } else {
                    $groupId = $group->getExternalId();
                }

                if($group && !empty($groupId)) {
                    $data = [
                        "attributeSetId"   => $attributeSetId,
                        "attributeGroupId" => $groupId,
                        "attributeCode"    => $attributeCode,
                        "sortOrder"        => $key
                    ];
                    $result = $this->addAttributeToAttributeSet($data);
                }
            }
        }
    }

    protected function getAttributesInAttributeSet($attributeSetId)
    {
        $method = 'GET';
        $url = $this->getApiUrlByEndpoint('getAttributeSet');
        $url = str_replace('{attributeSetId}', $attributeSetId, $url);

        try {
            $this->oauthClient->fetch($url, null, $method, $this->jsonHeaders );
            $results = json_decode($this->oauthClient->getLastResponse(), true);

        } catch(\Exception $e) {
            $results = [];
        }

        $codes = [];
        foreach($results as $result) {
            $codes[] = $result['attribute_code'];

        }

        return $codes;
    }

    protected function addGroup(array $resource)
    {
        $method = 'POST';
        $url = $this->getApiUrlByEndpoint('addAttributeGroup');

        try {
            $this->oauthClient->fetch($url, json_encode($resource), $method, $this->jsonHeaders );
            $results = json_decode($this->oauthClient->getLastResponse(), true);

            return $results;
        } catch(\Exception $e) {
            $error = ['error' => json_decode($this->oauthClient->getLastResponse(), true) ];
            return $error;
        }
    }

    protected function addAttributeToAttributeSet($data)
    {
        $url = $this->getApiUrlByEndpoint('addToAttributeSet');
        $method = 'POST';
        try {
            $this->oauthClient->fetch($url, json_encode($data), $method, $this->jsonHeaders );
            $results = json_decode($this->oauthClient->getLastResponse(), true);
            return $results;
        } catch(\Exception $e) {
        }
    }

    protected function checkAttributeSetFromApi($attributeSetId)
    {
        $url = $this->getApiUrlByEndpoint('updateAttributeSet');
        $url = str_replace('{attributeSetId}', $attributeSetId, $url);

        $method = 'GET';
        try {
            $this->oauthClient->fetch($url, null, $method, $this->jsonHeaders );
            $results = json_decode($this->oauthClient->getLastResponse(), true);
        } catch(\Exception $e) {
            $results = null;
        }

        return !empty($results);
    }


    /* post attributes to api */
    protected function postAttributeSetToApi(array $attributeSet, $action = 'add', $attributeId = null)
    {
        if('update' == $action ) {
            $url = $this->getApiUrlByEndpoint('updateAttributeSet');
            $url = str_replace('{attributeSetId}', $attributeId, $url);
            $method = 'PUT';
        } else {
            $url = $this->getApiUrlByEndpoint('addAttributeSet');
            $method = 'POST';
        }

        try {
            $this->oauthClient->fetch($url, json_encode($attributeSet), $method, $this->jsonHeaders );
            $results = json_decode($this->oauthClient->getLastResponse(), true);
            return $results;
        } catch(\Exception $e) {

            $lastResponse = json_decode($this->oauthClient->getLastResponse(), true);
            $responseInfo = $this->oauthClient->getLastResponseInfo();
            foreach(array_keys($responseInfo) as $key ) {
                if(trim($key) == 'http_code') {
                    $lastResponse['http_code'] = $responseInfo[$key];
                    break;
                }
            }

            $error = ['error' => $lastResponse ];
            return $error;
        }

        return null;
    }

    private function getAndAddAttributeSets()
    {
        $url = $this->getApiUrlByEndpoint('getAttributeSets');
        $method = 'GET';

        try {
            $this->oauthClient->fetch($url, null, $method, $this->jsonHeaders );
            $results = json_decode($this->oauthClient->getLastResponse(), true);
        } catch(\Exception $e) {
            $results = [];
        }

        if(!empty($results['items'])) {
            foreach($results['items'] as $result) {
                $this->updateMappingByCode($result['attribute_set_name'], $result['attribute_set_id'], $result['entity_type_id']);
            }
        }
    }

    protected function getAttributeGroupByName($name, $attributeSetId)
    {
        $url = strstr($this->getApiUrlByEndpoint('getAttributeGroup'), '?', true);
        $method = 'GET';

        $url .= '?' . 'searchCriteria[filter_groups][0][filters][0][field]=attribute_set_id&searchCriteria[filter_groups][0][filters][0][value]='
                . $attributeSetId .  '&searchCriteria[filter_groups][0][filters][0][condition_type]=eq'
                . '&searchCriteria[filter_groups][1][filters][1][field]=attribute_group_name&searchCriteria[filter_groups][1][filters][1][value]='
                . $name . '&searchCriteria[filter_groups][1][filters][1][condition_type]=eq';
        try {
            $this->oauthClient->fetch($url, null, $method, $this->jsonHeaders );
            $results = json_decode($this->oauthClient->getLastResponse(), true);

        } catch(\Exception $e) {
            $lastResponse = json_decode($this->oauthClient->getLastResponse(), true);
            $results = ['error' => $lastResponse ];
        }

        return !empty($results['items'][0]) ? $results['items'][0] : $results;
    }

    protected function getDefaultAttributeSetId()
    {
        if(empty($this->defaultAttributeSetId)) {
            $url = $this->getApiUrlByEndpoint('getAttributeSets');
            $url = strstr($url, '?', true) . '?searchCriteria[pageSize]=3';
            $method = 'GET';

            try {
                $this->oauthClient->fetch($url, null, $method, $this->jsonHeaders );
                $results = json_decode($this->oauthClient->getLastResponse(), true);
            } catch(\Exception $e) {
            }
            $defaultValue = null;
            if(isset($results)) {
                foreach($results['items'] as $resultItem) {
                    if($defaultValue) {
                        if($defaultValue > $resultItem['entity_type_id']) {
                            $defaultValue = $resultItem['entity_type_id'];
                        }
                    } else {
                        $defaultValue = $resultItem['entity_type_id'];
                    }
                }
            }
            $this->defaultAttributeSetId = $defaultValue ? : $this->fallbackAttributesetValue;
        }

        return $this->defaultAttributeSetId;
    }

    private function getGroupMappingByCode($code, $attributeSetId)
    {
        $mapping = $this->mappingRepository->findOneBy([
            'code' => $code,
            'entityType' => 'group',
            'relatedId' => $attributeSetId,
            'apiUrl' => $this->getApiUrl(),
        ]);

        return $mapping;
    }
}
