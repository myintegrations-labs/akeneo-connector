<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Connector\Writer;

use Akeneo\Component\Batch\Item\ItemWriterInterface;
use Akeneo\Component\Batch\Step\StepExecutionAwareInterface;
use Akeneo\Component\Batch\Model\StepExecution;
use Akeneo\Component\Batch\Item\DataInvalidItem;
use MyIntegrations\Bundle\ConnectorBundle\Connector\Writer\Model;
use MyIntegrations\Bundle\ConnectorBundle\Component\Normalizer\PropertiesNormalizer;
use MyIntegrations\Bundle\ConnectorBundle\Entity\DataMapping;
use MyIntegrations\Bundle\ConnectorBundle\Services\ApiConnector;
use Symfony\Component\HttpFoundation\Response;
use MyIntegrations\Bundle\ConnectorBundle\Traits\ChannelAwareTrait;
use MyIntegrations\Bundle\ConnectorBundle\Traits\DataMappingTrait;

class CategoryWriter extends BaseWriter implements ItemWriterInterface
{
    const AKENEO_ENTITY_NAME = 'category';
    const ERROR_DUPLICATE_URL_KEY = 'URL key for specified store already exists.';
    const ERROR_ENTITY_DELETED = 'No such entity with %fieldName = %fieldValue';

    use ChannelAwareTrait;
    use DataMappingTrait;

    protected $defaultStoreViewCode;

    public function __construct(\Doctrine\ORM\EntityManager $em, ApiConnector $connectorService, $channelRepo)
    {
        $this->em = $em;
        $this->connectorService = $connectorService;
        $this->channelRepo = $channelRepo;
    }

    /**
     * write category to magento2 Api
     * add category add translation for different store views using different request
     * @param array $items
     */
    public function write(array $items)
    {
        $parameters = $this->getParameters();
        $scope = $this->getChannelScope($this->stepExecution);
        $availableLocales = $this->getFilterLocales($this->stepExecution);
        $rootCategoryCode = $this->getDefaultCategoryTreeCode($parameters);
        $this->storeMapping = array_filter($this->getStoreMapping() );
        $locales = array_intersect($availableLocales, array_keys($this->storeMapping));

        var_dump($items);


        while(count($items)) {
            $errorMsg = false;
            $item = array_shift($items);
            var_dump($item);
            var_dump($rootCategoryCode);
            if($item['code'] == $rootCategoryCode) {
                $category = $this->getDefaultStoreCategory();

                var_dump('asdsadasd');
                if($category) {
                    $this->updateMappingByCode($item['code'], $category['id'], $category['parent_id']);
                }
                continue;
            }

            $locale = in_array($this->defaultLocale, array_keys($item['labels'])) ? $this->defaultLocale : array_keys($item['labels'])[0];
            $name = !empty($item['labels'][$locale]) ? $item['labels'][$locale] : null;

            $this->storeViewCode = $this->storeMapping[$locale];
            if(count($this->storeMapping)) {
                $this->defaultStoreViewCode = reset($this->storeMapping);
            }

            if(!$name) {
                /* show error if translation of category for locale is not present */
                $errorMsg = 'Untranslated category with code: ' . $item['code']  .  ', for locale: ' . $locale ;
                $this->stepExecution->addWarning($errorMsg , ['error' => true ], new DataInvalidItem([]));
                continue;
            }

            $item['name'] = $name;
            $mapping = $this->getMappingByCode($item['code']);

            /* format data */
            $data = $this->createArrayFromDataAndMatcher(
                        $item, $this->matcher , self::AKENEO_ENTITY_NAME
                    );
            /* add filler attributes */
            $data[self::AKENEO_ENTITY_NAME] = array_merge(
                                    $data[self::AKENEO_ENTITY_NAME],
                                    $this->filler
                                    );


            if(!$mapping) {
                /* mapping doesn't' exists */
                $data[self::AKENEO_ENTITY_NAME]['include_in_menu'] = true;
                if($item['parent']) {
                    $parentMapping = $this->getMappingByCode($item['parent']);
                    if($parentMapping) {
                        $data[self::AKENEO_ENTITY_NAME]['parent_id'] = $parentMapping->getExternalId();
                    } else {
                        /* no reference found */
                    }
                } else {
                    /* add to default category */
                    $data[self::AKENEO_ENTITY_NAME]['parent_id'] = 0;
                }
                $data[self::AKENEO_ENTITY_NAME]['custom_attributes'] = [
                    'attribute_code' => 'url_key',
                    'value'          => $item['code'],
                ];

                $category = $this->addCategory($data);

                if($category && empty($category['error'])) {
                    $this->addMappingByCode($item['code'], $category['id'], $category['parent_id']);
                } elseif(!empty($category['error']['parameters'][0]) /** && self::ERROR_DUPLICATE_URL_KEY == $category['error']['parameters'][0] **/ ) {
                    $this->getCategoriesAndAddMappings();
                    $mapping = $this->getMappingByCode($item['code']);

                    if(!$mapping) {
                        $this->stepExecution->addWarning(
                            $category['error']['parameters'][0],
                            [],
                            new DataInvalidItem(['code' => $item['code'] ])
                        );
                    }
                }
            }

            /* mapping exists */
            if($mapping) {
                if($mapping->getExternalId()) {
                    $data[self::AKENEO_ENTITY_NAME]['id'] = $mapping->getExternalId();
                    $data[self::AKENEO_ENTITY_NAME]['parent_id'] = $mapping->getRelatedId();
                    $category = $this->addCategory($data, $mapping->getExternalId());

                    if(!empty($category['error']['http_code']) && $category['error']['http_code'] == RESPONSE::HTTP_NOT_FOUND) {
                       $category = $this->handleDeletedEntity($item, $data, $mapping);
                    }
                }
            }

            if(!empty($category)) {
                $this->addCategoryTranslations($category, $item, $locales, $data);
            }

            if(!$errorMsg) {
                /* increment write count */
                $this->stepExecution->incrementSummaryInfo('write');
            }
        }
    }

    /**
    * when resource is deleted from magento, recrete category and add translations
    *
    * @param array $item 'Base item''
    * @param array $data 'formatted data'
    * @param DataMapping $mapping 'existing mapping'
    * @return array $category 'recreated category'
    */
    protected function handleDeletedEntity($item, $data, $mapping)
    {
        unset($data[self::AKENEO_ENTITY_NAME]['id']);
        unset($data[self::AKENEO_ENTITY_NAME]['parent_id']);
        $this->em->remove($mapping);
        $this->em->flush();
        $parentMapping = $this->getMappingByCode($item['parent']);
        if($parentMapping) {
            $data[self::AKENEO_ENTITY_NAME]['parent_id'] = $parentMapping->getExternalId();
        } else {
        }
        $category = $this->addCategory($data);

        if(!empty($category)) {
            $this->addCategoryTranslations($category, $item, $locales, $data);
        }

        if($category && empty($category['error']) ) {
            $this->addMappingByCode($item['code'], $category['id'], $category['parent_id']);
        }

        return $category;
    }

    /* fetch default store categroy from api */
    protected function getDefaultStoreCategory()
    {
        var_dump($this->storeViewCode);
        $url = $this->getApiUrlByEndpoint('categories', $this->storeViewCode) ;
        $url = strstr($url, '?', true) . '?depth=0';
        $method = 'GET';


        var_dump($url);

        try {
            $this->oauthClient->fetch($url, null, $method, $this->jsonHeaders );
            $results = json_decode($this->oauthClient->getLastResponse(), true);

            var_dump($results);

            return $results;
        } catch(\Exception $e) {
            $lastResponse = json_decode($this->oauthClient->getLastResponse(), true);
            $error = ['error' => $lastResponse ];

            return $error;
        }
    }

    /* add category according to current store view */
    protected function addCategory(array $category, $categoryId = null)
    {
        $storeViewCode = $this->storeViewCode && $this->storeViewCode != $this->defaultStoreViewCode ?
                            $this->storeViewCode :
                            '';

        if($categoryId) {
            $url = $this->getApiUrlByEndpoint('updateCategory', $storeViewCode);
            $url = str_replace('{category}', $categoryId, $url);
            $method = 'PUT';
        } else {
            $url = $this->getApiUrlByEndpoint('categories', $storeViewCode);
            $method = 'POST';
        }

        try {
            $this->oauthClient->fetch($url, json_encode($category), $method, $this->jsonHeaders );
            $results = json_decode($this->oauthClient->getLastResponse(), true);

            return $results;
        } catch(\Exception $e) {
            $lastResponse = json_decode($this->oauthClient->getLastResponse(), true);
            $responseInfo = $this->oauthClient->getLastResponseInfo();
            foreach(array_keys($responseInfo) as $key ) {
                if(trim($key) == 'http_code') {
                    $lastResponse['http_code'] = $responseInfo[$key];
                    break;
                }
            }

            $error = ['error' => $lastResponse ];

            return $error;
        }
    }

    /* add translations of category store view wise */
    protected function addCategoryTranslations($category, $item, $locales, $data)
    {
        if($category && empty($category['error'])) {
            foreach($locales as $locale) {
                if($locale !== $this->defaultLocale && !empty($item['labels'][$locale]) ) {
                    $this->storeViewCode = $this->storeMapping[$locale];
                    $data[self::AKENEO_ENTITY_NAME]['name'] = $item['labels'][$locale];

                    $mapping = $this->getMappingByCode($item['code']);
                    if($mapping) {
                        if($mapping->getExternalId()) {
                            $data[self::AKENEO_ENTITY_NAME]['id'] = $mapping->getExternalId();
                            $data[self::AKENEO_ENTITY_NAME]['parent_id'] = $mapping->getRelatedId();
                            $tCategory = $this->addCategory($data, $mapping->getExternalId());
                        }
                    } else {
                        $data[self::AKENEO_ENTITY_NAME]['include_in_menu'] = true;
                        if($item['parent']) {
                            $parentMapping = $this->getMappingByCode($item['parent']);
                            if($parentMapping) {
                                $data[self::AKENEO_ENTITY_NAME]['parent_id'] = $parentMapping->getExternalId();
                            } else {
                                /* no reference found */
                            }
                        } else {
                            /* add to default category */
                            $data[self::AKENEO_ENTITY_NAME]['parent_id'] = 0;
                        }
                        $data[self::AKENEO_ENTITY_NAME]['custom_attributes'] = [
                            'attribute_code' => 'url_key',
                            'value'          => $item['code'],
                        ];

                        $tCategory = $this->addCategory($data, $category['id']);

                        if($category && empty($category['error'])) {
                            $this->addMappingByCode($item['code'], $category['id'], $category['parent_id']);
                        }
                    }

                }
            }
        }
    }

    protected function getCategoriesAndAddMappings()
    {
        $url = $this->getApiUrlByEndpoint('categories');
        $method = 'GET';

        try {
            $this->oauthClient->fetch($url, null, $method, $this->jsonHeaders );
            $results = json_decode($this->oauthClient->getLastResponse(), true);
        } catch(\Exception $e) {
            $results = [];
        }

        $this->addCategoryMappingByChildrenData($results);
    }

    private function addCategoryMappingByChildrenData($resource)
    {
        if(!empty($resource['children_data'])) {
            foreach($resource['children_data'] as $result) {
                $this->updateMappingByCode($result['name'], $result['id'], $result['parent_id']);
                if(!empty($result['children_data'])) {
                    /* recursive call */
                    $this->addCategoryMappingByChildrenData($result['children_data']);
                }
            }
        }
    }

    protected $matcher = [
        'name'   => 'name'
    ];

    protected $filler = [
        'is_active' => 1
    ];
}
