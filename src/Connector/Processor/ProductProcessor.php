<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Connector\Processor;

use Akeneo\Component\Batch\Item\DataInvalidItem;
use Akeneo\Component\Batch\Job\JobParameters;
use Akeneo\Component\Batch\Model\StepExecution;
use Akeneo\Component\StorageUtils\Detacher\ObjectDetacherInterface;
use Pim\Component\Catalog\Repository\AttributeRepositoryInterface;
use Pim\Component\Catalog\Repository\ChannelRepositoryInterface;
use Pim\Component\Catalog\ValuesFiller\EntityWithFamilyValuesFillerInterface;
use Pim\Component\Connector\Processor\BulkMediaFetcher;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Pim\Component\Connector\Processor\Normalization\ProductProcessor as PimProductProcessor;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use MyIntegrations\Bundle\ConnectorBundle\Traits\FileInfoTrait;
use MyIntegrations\Bundle\ConnectorBundle\Traits\StepExecutionTrait;
use MyIntegrations\Bundle\ConnectorBundle\Component\Normalizer\PropertiesNormalizer as Prop;
use Pim\Component\Catalog\Model\ProductModel;

class ProductProcessor extends PimProductProcessor
{
    use FileInfoTrait;
    use StepExecutionTrait;

    /** @var NormalizerInterface */
    protected $normalizer;

    /** @var ChannelRepositoryInterface */
    protected $channelRepository;

    /** @var AttributeRepositoryInterface */
    protected $attributeRepository;

    /** @var ObjectDetacherInterface */
    protected $detacher;

    /** @var StepExecution */
    protected $stepExecution;

    /** @var BulkMediaFetcher */
    protected $mediaFetcher;

    /** @var EntityWithFamilyValuesFillerInterface */
    protected $productValuesFiller;

    protected $magentoBaseValuesArray = [
        'id','sku', 'name', 'attribute_set_id', 'price', 'status', 'updated_at', 'weight', 'media_gallery_entries',
        'quantity',
        'description', 'short_description', 'meta_title', 'meta_keyword', 'meta_description', 'url_key',
        'news_from_date', 'news_to_date', 'special_price', 'special_from_date', 'special_to_date'
        // 'visibility'
    ];

    protected $magentoNoncustomValuesArray = [
        'categories', 'axes', 'variants', 'allVariantAttributes'
    ];

    protected $router;

    protected $connectorService;

    /**
     * @param NormalizerInterface                   $normalizer
     * @param ChannelRepositoryInterface            $channelRepository
     * @param AttributeRepositoryInterface          $attributeRepository
     * @param ObjectDetacherInterface               $detacher
     * @param BulkMediaFetcher                      $mediaFetcher
     * @param EntityWithFamilyValuesFillerInterface $productValuesFiller
     */
    public function __construct(
        NormalizerInterface $normalizer,
        ChannelRepositoryInterface $channelRepository,
        AttributeRepositoryInterface $attributeRepository,
        ObjectDetacherInterface $detacher,
        BulkMediaFetcher $mediaFetcher,
        EntityWithFamilyValuesFillerInterface $productValuesFiller,
        Router $router,
        $connectorService
    ) {
        $this->normalizer = $normalizer;
        $this->detacher = $detacher;
        $this->channelRepository = $channelRepository;
        $this->attributeRepository = $attributeRepository;
        $this->mediaFetcher = $mediaFetcher;
        $this->productValuesFiller = $productValuesFiller;
        $this->router = $router;
        $this->connectorService = $connectorService;
    }

    /**
     * {@inheritdoc}
     */
    public function process($product, $recursiveCall = false)
    {
        if(!$recursiveCall && $product instanceof ProductModel) {
            /* skip excess ProductModel */
            return;
        }

        $parameters = $this->stepExecution->getJobParameters();

        if($scope = $this->getChannelScope($this->stepExecution)) {
            $channel = $this->channelRepository->findOneByIdentifier($scope);
        }

        if(!$recursiveCall) {
            $this->productValuesFiller->fillMissingValues($product);

            $productStandard = $this->normalizer->normalize($product, 'standard', [
                'channels' => !empty($channel) ? [$channel->getCode()] : [],
                'locales'  => array_intersect(
                    !empty($channel) ? $channel->getLocaleCodes() : [],
                    $this->getFilterLocales($this->stepExecution)
                ),
            ]);
        } else {
            $productStandard = $product;
        }

        if(!$recursiveCall && !empty($productStandard['parent'])) {
            $parentProductStandard = $productStandard['parent'];
            $productStandard['parent'] = $this->process($parentProductStandard, true);
        }

        $this->fillProductValues($productStandard);
        if ($this->areAttributesToFilter($parameters)) {
            $attributesToFilter = $this->getAttributesToFilter($parameters);
            $productStandard['values'] = $this->filterValues($productStandard['values'], $attributesToFilter);
        }
        if(isset($productStandard['associations'])) {
            unset($productStandard['associations']);
        }
        ///////////// custom code start //////////////

        /* images */
        if ($parameters->has('with_media') && $parameters->get('with_media')) {
            $mediaAttributes = $this->attributeRepository->findMediaAttributeCodes();
            $productStandard['media_gallery_entries'] = [];
            foreach($mediaAttributes as $mediaAttribute) {
                if($this->isSameLevelImage($mediaAttribute, $productStandard)) {
                    if(!empty($productStandard['values'][$mediaAttribute])) {
                        $convertedImage = $this->convertRelativeUrlToBase64(
                            $productStandard['values'][$mediaAttribute],
                            (!empty($productStandard['name']) ? $productStandard['name'] : ''),
                            $mediaAttribute == $productStandard[Prop::FIELD_META_DATA][Prop::ATTRIBUTE_AS_IMAGE] ? 0 : count($productStandard['media_gallery_entries'])
                            );
                        if($convertedImage) {
                            $productStandard['media_gallery_entries'][] = $convertedImage;
                        }
                    }
                }
                unset($productStandard['values'][$mediaAttribute]);
            }
        }

        /* add attribute to process */
        $attributeMappings = $this->connectorService->getAttributeMappings();
        $flippedMappings = array_flip($attributeMappings);
        $productStandard[Prop::FIELD_META_DATA]['unprocessed'] = [];
        $productStandard['custom_attributes'] = [];
        if(!empty($productStandard[Prop::FIELD_META_DATA][Prop::FIELD_IDENTIFIER])) {
            $productStandard['sku'] = $productStandard[Prop::FIELD_META_DATA][Prop::FIELD_IDENTIFIER];
        }

        /* custom attributes indexing */
        foreach($productStandard['values'] as $field => $value) {
            if(!$recursiveCall || $this->isSameLevelAttribute($field, $productStandard)) {
                if($field == 'sku') {
                    $productStandard['sku'] = $this->formatValueForMagento($value);
                } else if(in_array($field, array_values($attributeMappings))) {
                    if(isset($flippedMappings[$field]) && in_array($flippedMappings[$field], $this->magentoBaseValuesArray)) {
                        switch($flippedMappings[$field]) {
                            case 'quantity':
                                $formattedVal = $this->formatValueForMagento($value);
                                if(!is_array($formattedVal)) {
                                    $productStandard['extension_attributes'] = [
                                        'stock_item' => [
                                            'qty' => (string)(int)$formattedVal,
                                            'is_in_stock' => (boolean)$formattedVal,
                                            'manage_stock' => true,
                                        ]
                                    ];
                                }
                                break;
                            case 'news_to_date':
                            case 'news_from_date':
                            case 'description':
                            case 'short_description':
                            case 'meta_title':
                            case 'meta_keyword':
                            case 'meta_description':
                            case 'url_key':
                            case 'special_price':
                            case 'special_from_date':
                            case 'special_to_date':
                                $productStandard['custom_attributes'][] = [
                                    'attribute_code' => $flippedMappings[$field],
                                    'value'          => $value,
                                ];
                                break;
                            default:
                                $productStandard[$flippedMappings[$field]] = $value;
                                $productStandard[Prop::FIELD_META_DATA]['unprocessed'][] = $flippedMappings[$field];
                        }
                    }
                } else if(in_array($field, $this->magentoBaseValuesArray) ) {
                    $productStandard[$field] = $value;
                    $productStandard[Prop::FIELD_META_DATA]['unprocessed'][] = $field;
                } else if(!in_array($field, $this->magentoNoncustomValuesArray)) {
                    $customValue = [
                        'attribute_code' => $field,
                        'value'          => $value,
                    ];
                    $productStandard['custom_attributes'][] = $customValue;
                } else if(in_array($field, $this->magentoNoncustomValuesArray)) {
                    $productStandard[Prop::FIELD_META_DATA][$field] =  $value;
                    unset($productStandard[$field]);
                }
            }
        }

        unset($productStandard['values']);

        if(!$recursiveCall) {
            $this->detacher->detach($product);
        }

        return $productStandard;
    }

    /**
     * {@inheritdoc}
     */
    public function setStepExecution(StepExecution $stepExecution)
    {
        $this->stepExecution = $stepExecution;
    }

    protected function isSameLevelImage($attrCode, $productStandard)
    {
        if(!empty($productStandard[Prop::FIELD_PARENT]) ) {
            $childLevelAttribute = $productStandard[Prop::VARIANT_ATTRIBUTES];
            $flag = in_array($attrCode, $childLevelAttribute);
        } else {
            $flag = $this->isSameLevelAttribute($attrCode, $productStandard);
        }

        return $flag;
    }

    protected function isSameLevelAttribute($attrCode, $productStandard)
    {
        $flag = isset($productStandard[Prop::VARIANT_ATTRIBUTES]) && !in_array($attrCode, $productStandard[Prop::VARIANT_ATTRIBUTES]);

        return !$flag;
    }


    /**
     * Filters the attributes that have to be exported based on a product and a list of attributes
     *
     * @param array $values
     * @param array $attributesToFilter
     *
     * @return array
     */
    protected function filterValues(array $values, array $attributesToFilter)
    {
        $valuesToExport = [];
        $attributesToFilter = array_flip($attributesToFilter);
        foreach ($values as $code => $value) {
            if (isset($attributesToFilter[$code])) {
                $valuesToExport[$code] = $value;
            }
        }

        return $valuesToExport;
    }

    private function formatValueForMagento($value)
    {
        if(is_array($value)) {
            foreach($value as $key => $aValue) {
                if(is_array($aValue) ) {
                    if(array_key_exists('locale', $aValue)) {
                        if(!$aValue['locale'] || $aValue['locale'] == $this->locale) {
                            $value = $aValue['data'];
                            break;
                        }
                        if($key == count($value)-1) {
                            $value = null;
                        }
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        if($value && is_array($value) ) {
            /* price */
            foreach($value as $key => $aValue) {
                if(is_array($aValue)) {
                    if(array_key_exists('currency', $aValue)) {
                        if(!$aValue['currency'] || $aValue['currency'] == $this->baseCurrency) {
                            $value = $aValue['amount'];
                            break;
                        }
                        if($key == count($value)-1) {
                            $value = !empty($value[0]['amount']) ? $value[0]['amount'] : null ;
                        }
                    }

                } else {
                    break;
                }
            }
            /* metric */
            if(is_array($value) && array_key_exists('unit', $value)) {
                $value = !empty($value['amount']) ? $value['amount'] : null;
            }
        }

        return $value;
    }

    protected function convertRelativeUrlToBase64($entry, $productName = '', $position = 0)
    {
        if(is_array($entry)) {
            if(!empty($entry[0]['data']) ) {
                $entry = $entry[0]['data'];
            } else {
                return;
            }
        }

        $filename = explode('/', $entry);
        $filename = end($filename);
        try {
            // $context = $this->router->getContext();
            // $context->setHost('magento.demo.myintegrations.nu');
            $imageUrl = $this->router->generate('pim_enrich_media_show', [
                                        'filename' => urlencode($entry), 'filter' => 'thumbnail'
                                    ], UrlGeneratorInterface::ABSOLUTE_URL) ;

            $imageContent = file_get_contents($imageUrl);
            $mimetype = $this->getImageMimeType($imageContent);
            $imageContent = base64_encode($imageContent);

        } catch(\Exception $e) {
            return;
        }

        $convertedItem = [
            'media_type' => 'image',
            'label'      => $productName,
            'position'   => $position,
            'disabled'   => false,
            'types'      => $position ? [ "image" ] : [ "image", "small_image", "thumbnail" ],
            'content'    => [
                'base64_encoded_data' => $imageContent ,
                'type'                => $this->guessMimetype($mimetype) ? : 'image/png',
                'name'                => $filename,
            ],
        ];

        return $convertedItem;
    }

    /**
     * Return a list of attributes to export
     *
     * @param JobParameters $parameters
     *
     * @return array
     */
    protected function getAttributesToFilter(JobParameters $parameters)
    {
        $attributes = $parameters->get('filters')['structure']['attributes'];
        $identifierCode = $this->attributeRepository->getIdentifierCode();
        if (!in_array($identifierCode, $attributes)) {
            $attributes[] = $identifierCode;
        }

        return $attributes;
    }

    /**
     * Are there attributes to filters ?
     *
     * @param JobParameters $parameters
     *
     * @return bool
     */
    protected function areAttributesToFilter(JobParameters $parameters)
    {
         return isset($parameters->get('filters')['structure']['attributes'])
            && !empty($parameters->get('filters')['structure']['attributes']);
    }

    /* add value fillers for product */
    protected function fillProductValues(&$product)
    {
        $product['attribute_set_id']  = 4;;
    }

    protected function guessMimetype($ext)
    {
        $mimeArray = [
            'png'  => 'image/png',
            'jpe'  => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg'  => 'image/jpeg',
            'gif'  => 'image/gif',
            'bmp'  => 'image/bmp',
            'ico'  => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif'  => 'image/tiff',
            'svg'  => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
            'psd' => 'image/vnd.adobe.photoshop',
        ];

        return !empty($mimeArray[$ext]) ? $mimeArray[$ext] : '';
    }
}
