<?php

namespace MyIntegrations\Bundle\ConnectorBundle\Versioning;

use Doctrine\ORM\EntityManager;
use Pim\Bundle\VersioningBundle\UpdateGuesser\UpdateGuesserInterface;
use MyIntegrations\Bundle\ConnectorBundle\Versioning\VersionableInterface;

class UpdateGuesser implements UpdateGuesserInterface
{
    /**
     * {@inheritdoc}
     */
    public function supportAction($action)
    {
        return in_array(
            $action,
            [UpdateGuesserInterface::ACTION_UPDATE_ENTITY]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function guessUpdates(EntityManager $em, $entity, $action)
    {
        $pendings = [];
        if ($entity instanceof VersionableInterface) {
            $pendings[] = $entity;
        }

        return $pendings;
    }
}
